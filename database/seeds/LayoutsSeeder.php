<?php

use Illuminate\Database\Seeder;

class LayoutsSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        //about page

        DB::table('layouts')->insert([
            'name' => 'About',
            'type' => 'about',
            'content' => '{
            "who_we_are": {
              "image": "assets/about/who-we-are.png",
              "description": "<p>The time is now for it to be okay to be great. People in this world shun people for being great. For being a bright color. For standing out. But the time is now.</p><p><span style=\"font-size: 1rem;\">The time is now for it to be okay to be great. People in this world shun people for being great. For being a bright color. For standing out. But the time is now.</span></p><p><span style=\"font-size: 1rem;\">The time is now for it to be okay to be great. People in this world shun people for being great. For being a bright color. For standing out. But the time is now.</span></p>"
            },
            "core_business": {
              "image": "assets/about/core-business.png",
              "description": "<p>The time is now for it to be okay to be great. People in this world shun people for being great. For being a bright color. For standing out. But the time is now.</p><p>The time is now for it to be okay to be great. People in this world shun people for being great. For being a bright color. For standing out. But the time is now.</p><p>The time is now for it to be okay to be great. People in this world shun people for being great. For being a bright color. For standing out. But the time is now.<br></p>"
            },
            "our_achievements": {
              "image": "assets/about/our-achievements.jpg"
            },
            "our_affiliates": [
              "assets/affiliates/1.svg",
              "assets/affiliates/3.svg",
              "assets/affiliates/1.svg"
            ]
          }',
        ]);

        DB::table('layouts')->insert([
            'name' => 'Our Affiliates',
            'type' => 'about-our-affiliates',
            'image' => 'assets/affiliates/1.svg',
        ]);

        DB::table('layouts')->insert([
            'name' => 'Our Affiliates',
            'type' => 'about-our-affiliates',
            'image' => 'assets/affiliates/2.svg',
        ]);

        DB::table('layouts')->insert([
            'name' => 'Our Affiliates',
            'type' => 'about-our-affiliates',
            'image' => 'assets/affiliates/3.svg',
        ]);

        //contact us page

        DB::table('layouts')->insert([
            'name' => 'Temae Entertaiment',
            'type' => 'contact',
            'content' => '{
                "location": "Rumah Cisarua, Kec. Cisarua, Bogor, Jawa Barat",
                "phone": "(021) 22123888",
                "email": "info@temaeent.com",
                "embed_map": ""
            }',
        ]);

        //services page

        DB::table('services')->insert([
            'name' => 'IDN Times',
            'slug' => 'idn-times',
            'type' => 'services',
            'image' => 'assets/services/services-image.jpg',
            'logo' => 'assets/services/services-logo.png',
            'description' =>
                "<p>The time is now for it to be okay to be great. People in this world shun people for being great. For being a bright color. For standing out. But the time is now.</p><p><span style=\"font-size: 1rem;\">The time is now for it to be okay to be great. People in this world shun people for being great. For being a bright color. For standing out. But the time is now.</span></p><p><span style=\"font-size: 1rem;\">The time is now for it to be okay to be great. People in this world shun people for being great. For being a bright color. For standing out. But the time is now.</span></p>",
        ]);

        DB::table('services')->insert([
            'name' => 'IDN Times',
            'slug' => 'idn-times-2',
            'type' => 'services',
            'image' => 'assets/services/services-image.jpg',
            'logo' => 'assets/services/services-logo.png',
            
        ]);

        DB::table('services')->insert([
            'name' => 'Temae',
            'type' => 'portofolio',
            'image' => 'assets/services/1.jpg',
            'link' => 'https://www.instagram.com/temae.entertainment/'
        ]);

        DB::table('services')->insert([
            'name' => 'Temae',
            'type' => 'portofolio',
            'image' => 'assets/services/2.jpg',
            'link' => 'https://www.instagram.com/p/CQx5P3rsKPw/'
        ]);


        DB::table('services')->insert([
            'name' =>
                'AKHIRNYA ANDRE TAULANY BISA YOUTUBE BARENG VINCENT DESTA',
            'link' => 'https://www.youtube.com/watch?v=qLGeQbLR1cA',
            'type' => 'our-client',
        ]);

        DB::table('services')->insert([
            'name' =>
                'KENA COVID JADI MURTAD‼️ DJANCOK 🤣‼️ -Gus Miftah -Deddy Corbuzier Podcast',
            'link' => 'https://www.youtube.com/watch?v=9FXJgGf_T3A',
            'type' => 'our-client',
        ]);
    }
}
