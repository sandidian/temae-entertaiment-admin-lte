<?php

use Illuminate\Database\Seeder;

class SliderHomeSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('slider_home')->insert([
            'name' => 'lorem ipsum dolor sit amet',
            'order' => 1,
            'image' => 'assets/slider-home/wallpaperflare.com_wallpaper-1.jpg'
        ]);

        DB::table('slider_home')->insert([
            'name' => 'lorem ipsum dolor sit amet',
            'order' => 2,
            'image' => 'assets/slider-home/wallpaperflare.com_wallpaper-2.jpg'
        ]);

    }
}


?>