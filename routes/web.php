<?php

use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/foo', function () {
    Artisan::call('optimize:clear');
});

Route::get('/', 'Frontend\HomeController@index')->name('frontend.home');

Route::get('/about', 'Frontend\AboutUsController@index')->name(
    'frontend.about-us'
);
Route::get('/contact', 'Frontend\ContactUsController@index')->name(
    'frontend.contact-us'
);

Route::get('/videos', 'Frontend\VideosController@index')->name(
    'frontend.videos'
);
Route::get(
    '/videos/action/load-more-data',
    'Frontend\VideosController@more_data'
)->name('frontend.videos.load-more-data');
Route::get('/videos/{slug}', 'Frontend\VideosController@detail')->name(
    'frontend.videos.detail'
);

Route::get('/creators', 'Frontend\CreatorsController@index')->name(
    'frontend.creators'
);
Route::get(
    '/creators/action/load-more-data',
    'Frontend\CreatorsController@more_data'
)->name('frontend.creators.load-more-data');
Route::get('/creators/{slug}', 'Frontend\CreatorsController@detail')->name(
    'frontend.creators.detail'
);

Route::get('/services', 'Frontend\ServicesController@index')->name(
    'frontend.services'
);

Route::get(
    '/services/portofolio',
    'Frontend\ServicesController@portofolio'
)->name('frontend.services.portofolio');

Route::get('/services/{slug}', 'Frontend\ServicesController@detail')->name(
    'frontend.services.detail'
);

Auth::routes();

Route::prefix('manager')->group(function () {
    Route::get('/', 'Manager\HomeController@index')->name('home');

    Route::get('/profile', 'Manager\HomeController@profile')
        ->name('home.profile')
        ->middleware('auth');

    Route::post('/profile', 'Manager\HomeController@profileUpdate')
        ->name('home.profile.update')
        ->middleware('auth');

    Route::get('/change-password', 'Manager\HomeController@changePassword')
        ->name('home.change-password')
        ->middleware('auth');

    Route::post(
        '/change-password',
        'Manager\HomeController@changePasswordUpdate'
    )
        ->name('home.change-password.update')
        ->middleware('auth');

    Route::resource('users', 'Manager\UserController')->middleware('auth');
    Route::resource('slider-home', 'Manager\SliderHomeController')->middleware(
        'auth'
    );
    Route::resource('news', 'Manager\NewsController')->middleware('auth');
    Route::resource('creators', 'Manager\CreatorsController')->middleware(
        'auth'
    );

    Route::prefix('contact')->group(function () {
        Route::get('/', 'Manager\ContactController@index')
            ->middleware('auth')
            ->name('contact.index');
        Route::post('/', 'Manager\ContactController@update')
            ->middleware('auth')
            ->name('contact.update');
    });

    Route::prefix('about')->group(function () {
        Route::get('/who-we-are', 'Manager\About\WhoWeAreController@index')
            ->middleware('auth')
            ->name('who-we-are.index');
        Route::post('/who-we-are', 'Manager\About\WhoWeAreController@update')
            ->middleware('auth')
            ->name('who-we-are.update');

        Route::get(
            '/our-achievements',
            'Manager\About\OurAchievementsController@index'
        )
            ->middleware('auth')
            ->name('our-achievements.index');
        Route::post(
            '/our-achievements',
            'Manager\About\OurAchievementsController@update'
        )
            ->middleware('auth')
            ->name('our-achievements.update');

        Route::get(
            '/core-business',
            'Manager\About\CoreBusinessController@index'
        )
            ->middleware('auth')
            ->name('core-business.index');
        Route::post(
            '/core-business',
            'Manager\About\CoreBusinessController@update'
        )
            ->middleware('auth')
            ->name('core-business.update');

        Route::resource(
            '/our-affiliates',
            'Manager\About\OurAffiliatesController'
        )->middleware('auth');
    });

    Route::prefix('services')->group(function () {
        Route::resource(
            '/services',
            'Manager\Services\ServicesController'
        )->middleware('auth');

        Route::resource(
            '/portofolio',
            'Manager\Services\PortofolioController'
        )->middleware('auth');

        Route::resource(
            'our-client',
            'Manager\Services\OurClientController'
        )->middleware('auth');
    });
});
