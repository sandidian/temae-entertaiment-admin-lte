<div class="container container-lg mb-4">

    <div class="row align-items-center justify-content-md-between">
        <div class="col-md-6">
            <div class="copyright">
                &copy; {{ date('Y') }} Temae Entertaiment.
            </div>
        </div>
        <div class="col-md-6">
            <ul class="nav nav-footer justify-content-end">
                <li class="nav-item">
                    <a class="nav-link font-weight-bold" href="#" target="_blank">
                        <i class="fab fa-fw fa-tiktok fa-2x" aria-hidden="true"></i>
                    </a>
                </li>
                <li class="nav-item">
                    <a class="nav-link font-weight-bold" href="#" target="_blank">
                        <i class="fab fa-fw fa-youtube fa-2x" aria-hidden="true"></i>
                    </a>
                </li>
                <li class="nav-item">
                    <a class="nav-link font-weight-bold" href="#" target="_blank">
                        <i class="fab fa-fw fa-facebook fa-2x" aria-hidden="true"></i>
                    </a>
                </li>
                <li class="nav-item">
                    <a class="nav-link font-weight-bold" href="#" target="_blank">
                        <i class="fab fa-fw fa-twitter fa-2x" aria-hidden="true"></i>
                    </a>
                </li>
                <li class="nav-item">
                    <a class="nav-link font-weight-bold" href="#" target="_blank">
                        <i class="fab fa-fw fa-instagram fa-2x" aria-hidden="true"></i>
                    </a>
                </li>


            </ul>
        </div>
    </div>
</div>


<footer class="footer has-cards">

</footer>