<nav id="navbar-main"
    class="navbar navbar-main navbar-expand-lg {{ $navbarHome ? 'navbar-transparent' : 'bg-white position-sticky top-0 shadow' }}  navbar-light py-2">
    <div class="container container-lg">
        <a class="navbar-brand" href="/">
            <img src="{{ asset('assets/logo-black.png') }}">
        </a>
        <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbar-primary"
            aria-controls="navbar-primary" aria-expanded="false" aria-label="Toggle navigation">
            <span class="navbar-toggler-icon"></span>
        </button>
        <div class="collapse navbar-collapse" id="navbar-primary">
            <div class="navbar-collapse-header">
                <div class="row">
                    <div class="col-6 collapse-brand">
                        <a href="/">
                            <img src="{{ asset('assets/logo-black.png') }}">
                        </a>
                    </div>
                    <div class="col-6 collapse-close">
                        <button type="button" class="navbar-toggler" data-toggle="collapse"
                            data-target="#navbar-primary" aria-controls="navbar-primary" aria-expanded="false"
                            aria-label="Toggle navigation">
                            <span></span>
                            <span></span>
                        </button>
                    </div>
                </div>
            </div>
            <ul class="navbar-nav ml-lg-auto">
                <li class="nav-item">
                    <a class="nav-link font-weight-bold" href="/">Home
                        <span class="sr-only">(current)</span>
                    </a>
                </li>
                <li class="nav-item">
                    <a class="nav-link font-weight-bold" href="{{ route('frontend.about-us') }}">About Us</a>
                </li>
                <li class="nav-item">
                    <a class="nav-link font-weight-bold" href="{{ route('frontend.creators') }}">Creators</a>
                </li>
                <li class="nav-item">
                    <a class="nav-link font-weight-bold" href="{{ route('frontend.services') }}">Services</a>
                </li>
                <li class="nav-item">
                    <a class="nav-link font-weight-bold" href="{{ route('frontend.contact-us') }}">Contact Us</a>
                </li>
            </ul>
        </div>
    </div>
</nav>