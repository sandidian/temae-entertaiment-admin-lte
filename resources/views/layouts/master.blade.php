<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="utf-8" />
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <link rel="apple-touch-icon" sizes="76x76" href="{{ asset('vendor/frontend/assets/img/apple-icon.png') }}">
    <link rel="icon" type="image/png" href="{{ asset('favicon-2.ico') }}">
    <title>
        @yield('title') - Temae Entertaiment

    </title>
    <!-- Font Awesome Icons -->
    <link href="{{ asset('vendor/frontend/assets/fontawesome/css/fontawesome.css') }}" rel="stylesheet" />

    <link href="{{ asset('vendor/frontend/assets/fontawesome/css/brands.css') }}" rel="stylesheet">
    <link href="{{ asset('vendor/frontend/assets/fontawesome/css/solid.css') }}" rel="stylesheet">

    <!-- CSS Files -->
    <link href="{{ asset('vendor/frontend/assets/css/argon-design-system.css?v=1.2.2') }}" rel="stylesheet" />

    <link href="{{ asset('css/main.css') }}" rel="stylesheet" />

    @yield('css')

    <style>
    .footer.has-cards:before {

        background: none !important;
    }

    .footer.has-cards {
        padding-top: 620px !important;
        margin-top: -650px !important;

        background-image: url(<?php echo asset('assets/wave-footbar.png') ?>);
        background-size: 100%;
        background-repeat: no-repeat;
        z-index: -1;
        background-position: center bottom;

    }

    </style>
</head>

<body class="index-page">

    <!-- Navbar -->
    @include('layouts.navbar', $data)
    <!-- End Navbar -->

    @yield('content')

    @include('layouts.footer')
    </div>
    <!--   Core JS Files   -->
    <script src="{{ asset('vendor/frontend/assets/js/core/jquery.min.js') }}" type="text/javascript"></script>
    <script src="{{ asset('vendor/frontend/assets/js/core/popper.min.js') }}" type="text/javascript"></script>
    <script src="{{ asset('vendor/frontend/assets/js/core/bootstrap.min.js') }}" type="text/javascript"></script>
    <script src="{{ asset('vendor/frontend/assets/js/plugins/perfect-scrollbar.jquery.min.js') }}"></script>
    <!--  Plugin for Switches, full documentation here: http://www.jque.re/plugins/version3/bootstrap.switch/ -->
    <script src="{{ asset('vendor/frontend/assets/js/plugins/bootstrap-switch.js') }}"></script>
    <!--  Plugin for the Sliders, full documentation here: http://refreshless.com/nouislider/ -->
    <script src="{{ asset('vendor/frontend/assets/js/plugins/nouislider.min.js') }}" type="text/javascript"></script>
    <script src="{{ asset('vendor/frontend/assets/js/plugins/moment.min.js') }}"></script>
    <script src="{{ asset('vendor/frontend/assets/js/plugins/datetimepicker.js') }}" type="text/javascript"></script>
    <script src="{{ asset('vendor/frontend/assets/js/plugins/bootstrap-datepicker.min.js') }}"></script>
    <!-- Control Center for Argon UI Kit: parallax effects, scripts for the example pages etc -->
    <!--  Google Maps Plugin    -->
    <script src="{{ asset('vendor/frontend/assets/js/argon-design-system.min.js?v=1.2.2') }}" type="text/javascript">
    </script>
    @if($data['navbarHome'])
    <script>
    $(window).scroll(function() {
        var scroll = $(window).scrollTop();

        //>=, not <=
        if (scroll >= 50) {
            //clearHeader, not clearheader - caps H

            $("#navbar-main").removeClass("navbar-transparent");
            $("#navbar-main").addClass("bg-white position-sticky top-0 shadow");
            $("#navbar-main").fadeIn();


        } else {

            $("#navbar-main").addClass("navbar-transparent");
            $("#navbar-main").removeClass("position-sticky top-0 shadow");
            $("#navbar-main").fadeIn();

        }
    }); //missing );
    </script>
    @endif
    <script>
    function scrollToDownload() {

        if ($('.section-download').length != 0) {
            $("html, body").animate({
                scrollTop: $('.section-download').offset().top
            }, 1000);
        }
    }
    </script>

    @yield('js')
</body>

</html>