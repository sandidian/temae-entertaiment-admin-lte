@extends('layouts.master',['data' => ['navbarHome'=> false]])

@section('title','Contact Us')

@section('content')

<div class="wrapper" id="contactUs">

    <div class="section features-1">
        <div class="container container-lg">
            <div class="row">
                <div class="col-12 col-md-7">
                    <div class="display-1 font-gotham-bold">Contact Us</div>

                    <div>
                        <div class="d-flex flex-row">
                            <h3><i class="fa fa-fw fa-map-marker " aria-hidden="true"></i></h3>
                            <h4 class="px-3">
                                {{ $model->name }} <br>
                                {{ $contact['location'] }}
                            </h4>
                        </div>
                        <a class="d-flex flex-row" href="https://api.whatsapp.com/send?phone={{ $contact['phone'] }}"
                            target="_blank">
                            <h3><i class="fa fa-fw fa-phone " aria-hidden="true"></i></h3>
                            <h4 class="px-3">
                                {{ $contact['phone'] }}
                            </h4>
                        </a>
                        <a class="d-flex flex-row" href="mailto:{{ $contact['email'] }}" target="_blank">
                            <h3><i class="fa fa-fw fa-envelope " aria-hidden="true"></i></h3>
                            <h4 class="px-3">
                                {{ $contact['email'] }}
                            </h4>
                        </a>
                    </div>

                </div>
                <div class="col-12 col-md-5 position-relative">
                    <div class="position-absolute">
                        <div class="icons position-absolute" style="top: 34%;left: 15%;">
                            Follow Us
                        </div>
                        <div class="icons position-absolute" style="top: 46%;left: 8%;">
                            <div class="d-flex">
                                <div class="pr-2">
                                    <a href="#">
                                        <img src="{{ asset('assets/icons/instagram.png') }}" width="22px">
                                    </a>
                                </div>
                                <div class="px-2">
                                    <a href="#">
                                        <img src="{{ asset('assets/icons/tik-tok.png') }}" width="22px">
                                    </a>
                                </div>
                                <div class="px-2">
                                    <a href="#">
                                        <img src="{{ asset('assets/icons/linkedin.png') }}" width="22px">
                                    </a>
                                </div>
                                <div class="px-2">
                                    <a href="#">
                                        <img src="{{ asset('assets/icons/twitter.png') }}" width="22px">
                                    </a>
                                </div>
                            </div>
                        </div>
                        <img src="{{ asset('assets/bubble-chat.png') }}" width="50%" class="top">
                    </div>
                    <img src="{{ asset('assets/open-doodles-laying.png') }}" style="margin-top:150px" width="100%">
                </div>
            </div>
            <div class="row">
                <div class="col-md-12">
                    <iframe
                        src="https://www.google.com/maps/embed?pb=!1m14!1m8!1m3!1d3963.4747715444314!2d106.8147028!3d-6.5877553!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x2e69c54fb9063287%3A0xc67dcd1545db0659!2sTemae%20Entertainment!5e0!3m2!1sid!2sid!4v1631539378997!5m2!1sid!2sid"
                        width="100%" class="py-5" style="border:0;height:50rem;" allowfullscreen=""
                        loading="lazy"></iframe>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection