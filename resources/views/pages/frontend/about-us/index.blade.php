@extends('layouts.master',['data' => ['navbarHome'=> true]])

@section('title','About Us')

@section('css')
<link rel="stylesheet" href="{{ asset('vendor/owl-carousel/assets/owl.carousel.min.css') }}" />
<link rel="stylesheet" href="{{ asset('vendor/owl-carousel/assets/owl.theme.default.min.css') }}" />
@endsection

@section('content')

<div class="wrapper" id="aboutUs">
    <div class="position-relative text-left" id="aboutBanner">
        <img src="{{ asset('assets/services/4.jpg') }}" width="100%">
        <div class="title">
            <div class="text-white font-gotham-bold slider-hone-text text-center">Hallo, <b class="font-weight-bold"> We
                    Are</b></div>
            <div class="text-white font-gotham-bold slider-hone-text text-center"><b class="text-yellow"> Temae</b> Team
            </div>
        </div>
    </div>
    <div id="about-who-we-are">
        <div class="container container-lg">
            <div class="row">
                <div class="col-md-6">
                    <div class="name">
                        <h3 class="font-gotham-bold font-weight-bold title-section text-dark">Who</h3>
                        <h3 class="font-gotham-bold font-weight-bold title-section text-dark"
                            style="line-height:0.5!important">We Are</h3>

                    </div>
                    <div class="info">
                        <div class="description opacity-8 text-dark">
                            {!! $content['who_we_are']['description'] !!}
                        </div>
                    </div>
                </div>
                <div class="col-lg-6 col-12 mx-md-auto">
                    <img class="ml-lg-5" src="{{ url($content['who_we_are']['image']) }}" width="100%">
                </div>
            </div>
        </div>
    </div>

    <div class="container" id="about-news">

        <div class="row">

            <div class="col-12 col-md-4 title-mobile">
                <div class="text-center text-dark name-top">
                    What's
                </div>
                <div class="text-center text-dark name-bottom">
                    News?
                </div>
            </div>

            @if($news[0])
            <div class="col-12 col-md-4">
                <div class="position-relative">
                    <a href="{{ route('frontend.videos.detail',$news[0]->slug) }}">
                        <img width="100%"
                            src="https://img.youtube.com/vi/{{ youtubeID($news[0]->youtube) }}/mqdefault.jpg"
                            alt="First slide" style="object-fit: cover;" class="rounded">
                        <div class="title p-3 text-dark text-center news-title-left">
                            {{$news[0]->title}}
                        </div>
                    </a>
                </div>
            </div>
            @endif
            <div class="col-12 col-md-4 title-desktop">
                <div class="text-center text-dark name-top">
                    What's
                </div>
                <div class="text-center text-dark name-bottom">
                    News?
                </div>
                <a href="{{ route('frontend.videos') }}">
                    <div class="font-weight-bold text-uppercase text-center text-dark mt-n3 pb-4">
                        <u> View All</u>
                    </div>
                </a>
            </div>
            @if($news[1])
            <div class="col-12 col-md-4">
                <div class="position-relative">
                    <a href="{{ route('frontend.videos.detail',$news[1]->slug) }}">
                        <a href="{{ route('frontend.videos.detail',$news[1]->slug) }}">
                            <img width="100%"
                                src="https://img.youtube.com/vi/{{ youtubeID($news[1]->youtube) }}/mqdefault.jpg"
                                alt="First slide" style="object-fit: cover;" class="rounded">

                            <div class="title p-3 text-dark text-center news-title-right">
                                {{$news[1]->title}}
                            </div>
                        </a>
                </div>
            </div>
            @endif

            <div class="col-12 mt-4 view-all-mobile">
                <a href="{{ route('frontend.videos') }}">
                    <div class="font-weight-bold text-uppercase text-center text-dark mt-n3 pb-4">
                        <u> View All</u>
                    </div>
                </a>
            </div>

        </div>


    </div>

    <div id="about-core-business">
        <div class="container container-lg">
            <div class="row">
                <div class="col-12 col-md-4 icon-desktop">
                    <img src="{{ asset('assets/about/icon-social-media-management.png') }}" class="mx-auto d-block"
                        width="100%">
                    <div class="h5 text-center">
                        Social Media Management
                    </div>
                </div>
                <div class="col-12 col-md-8">
                    <div class="container-name">
                        <div class="text-dark font-gotham-bold font-weight-bold name-top">
                            Core</div>
                        <div class="text-dark font-gotham-bold font-weight-bold name-bottom">
                            Business</div>
                    </div>
                </div>
            </div>
            <div class="row">
                <div class="col-12 col-md-4 icon-mobile">
                    <img src="{{ asset('assets/about/icon-social-media-management.png') }}" class="mx-auto d-block"
                        width="100%">
                    <div class="h5 text-center text-dark">
                        Social Media Management
                    </div>
                </div>
                <div class="col-12 col-md-4">
                    <img src="{{ asset('assets/about/icon-kol-management.png') }}" class="mx-auto d-block" width="100%">
                    <div class="h5 text-center text-dark">
                        Kol Management
                    </div>
                </div>
                <div class="col-12 col-md-4">
                    <img src="{{ asset('assets/about/icon-ads-placement.png') }}" class="mx-auto d-block" width="100%">
                    <div class="h5 text-center text-dark">
                        Ads Placement
                    </div>
                </div>
                <div class="col-12 col-md-4">
                    <img src="{{ asset('assets/about/icon-creative-productine.png') }}" class="mx-auto d-block" width="100%">
                    <div class="h5 text-center text-dark">
                        Creative Production
                    </div>
                </div>
            </div>
        </div>
    </div>

    <div class="container mb-lg mt-md affiliates">
        <div class="row">
            <div class="col-12 mb-5">
                <div class="text-center">
                    <div class="display-1 font-gotham-bold title-section text-dark">Our Affiliates</div>
                </div>
            </div>
        </div>
        <div class="row">
            <div class="col-12 ">
                <div class="d-flex flex-wrap items">
                    @foreach($ourAffiliates as $key => $value)
                    <img src="{{ url($value->image) }}" class="col-6 col-md-4 p-3">
                    @endforeach
                </div>
            </div>
        </div>
    </div>
</div>
@endsection

@section('js')

<script src="{{ asset('vendor/owl-carousel/owl.carousel.min.js') }}"></script>

<script>
$(document).ready(function($) {


    $('#newsList').owlCarousel({
        items: 3,
        margin: 20,
        responsiveClass: true,
        navText: ["<i class='fa fa-fw fa-2x fa-chevron-left'>",
            "<i class='fa fa-fw fa-2x fa-chevron-right'>"
        ],
        responsive: {
            300: {
                items: 1,
                nav: true,
                dots: false
            },
            600: {
                items: 1,
                nav: true,
                dots: false
            },
            1000: {
                items: 3,
                nav: true,
                dots: false
            }
        }
    });

});
</script>
@endsection