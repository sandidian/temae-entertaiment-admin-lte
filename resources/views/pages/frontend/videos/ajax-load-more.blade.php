@foreach($models as $key => $model)

<div class="col-12 col-md-4 box-videos video-detail">
    <a href="{{ route('frontend.videos.detail',$model->slug) }}">
        <img src="https://img.youtube.com/vi/{{ youtubeID($model->youtube) }}/mqdefault.jpg" alt="First slide"
            width="100%">
    </a>
    <a href="{{ route('frontend.videos.detail',$model->slug) }}">
        <div class="font-weight-bold mt-2 text-dark">{{$model->title}}</div>
    </a>

</div>

@endforeach