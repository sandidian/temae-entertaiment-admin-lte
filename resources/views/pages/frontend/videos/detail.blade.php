@extends('layouts.master',['data' => ['navbarHome'=> false]])

@section('title', $model->title)

@section('css')
<style>
.videowrapper {
    float: none;
    clear: both;
    width: 100%;
    position: relative;
    padding-bottom: 56.25%;
    padding-top: 25px;
    height: 0;
}

.videowrapper iframe {
    position: absolute;
    top: 0;
    left: 0;
    width: 100%;
    height: 100%;
}

@media (min-width: 1200px) {
    .container {
        max-width: 900px !important;
    }
}
</style>
@endsection

@section('content')
<div class="wrapper" id="videos-detail">
    <div class="container">
        <div class="row">
            <div class="col-12 text-dark">
                <div class="videowrapper mt-3"> <iframe width="854" height="480"
                        src="https://www.youtube.com/embed/{{ youtubeID($model->youtube) }}" frameborder="0"
                        gesture="media" allowfullscreen> </iframe> </div>
                <div class="h4 mt-3">{{ $model->title }}</div>
                <div class="h6 mt-n1">{{ changeDateFormate($model->published_at,'l, d F  Y') }}</div>

                {!! $model->content !!}

            </div>
        </div>
        <div class="row">
            <div class="col-12">
                <div class="text-center name-new-update">
                    <div class="display-1 text-uppercase font-gotham-bold title-section">News Update</div>
                </div>
            </div>
        </div>
        <div class="row">

            @foreach($models as $key => $model)

            <div class="col-12 col-md-4 box-videos video-detail">
                <a href="{{ route('frontend.videos.detail',$model->slug) }}">
                    <img src="https://img.youtube.com/vi/{{ youtubeID($model->youtube) }}/mqdefault.jpg"
                        alt="First slide" width="100%">
                </a>
                <a href="{{ route('frontend.videos.detail',$model->slug) }}">
                    <div class="font-weight-bold mt-2 text-dark">{{$model->title}}</div>
                </a>

            </div>

            @endforeach

        </div>
    </div>
</div>
@endsection

@section('js')
@endsection