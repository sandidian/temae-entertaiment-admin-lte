@extends('layouts.master',['data' => ['navbarHome'=> false]])

@section('title','Videos')

@section('css')
@endsection

@section('content')
<div class="wrapper" id="videos">
    <div class="container container-lg mt-5 mb-5">

        <div class="row">
            <div class="col-md-8 mx-auto text-center">
                <h3 class="display-1 font-gotham-bold title-section">News List</h3>
            </div>
        </div>

        <div class="row" id="videosList">

            @foreach($models as $key => $model)

            <div class="col-12 col-md-4 box-videos video-detail">
                <a href="{{ route('frontend.videos.detail',$model->slug) }}">
                    <img src="https://img.youtube.com/vi/{{ youtubeID($model->youtube) }}/mqdefault.jpg"
                        alt="First slide" width="100%">
                </a>
                <a href="{{ route('frontend.videos.detail',$model->slug) }}">
                    <div class="font-weight-bold mt-2 text-dark">{{$model->title}}</div>
                </a>

            </div>

            @endforeach

        </div>

        @if(App\News::where(['status' => 1])->count() >= 6)

        <div class="row justify-content-md-center">
            <div class="col-12 col-md-3 ">
                <div class="btn btn-primary py-2 mt-5 col-12 load-more"
                    data-totalResult="{{ App\News::where(['status' => 1])->count() }}" style="cursor:pointer">
                    Load More</div>
            </div>
        </div>

        @endif

    </div>

</div>
@endsection

@section('js')
<script type="text/javascript">
$(document).ready(function() {
    $(".load-more").on('click', function() {
        var _totalCurrentResult = $(".box-videos").length;
        // Ajax Reuqest
        $.ajax({
            url: "{{ route('frontend.videos.load-more-data') }}",
            type: 'get',
            dataType: 'json',
            data: {
                skip: _totalCurrentResult
            },
            beforeSend: function() {
                $(".load-more").html('Loading...');
            },
            success: function(response) {

                $("#videosList").append(response.view);
                var _totalCurrentResult = $(".box-videos").length;
                var _totalResult = parseInt($(".load-more").attr(
                    'data-totalResult'));

                console.log([_totalCurrentResult, _totalResult])

                if (_totalCurrentResult == _totalResult) {
                    $(".load-more").remove();
                } else {
                    $(".load-more").html('Load More');
                }
            }
        });
    });
});
</script>
@endsection