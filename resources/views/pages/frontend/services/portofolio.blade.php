@extends('layouts.master',['data' => ['navbarHome'=> false]])

@section('title','Portofolio')

@section('content')

<div class="wrapper mt-4" id="servicesPortofolio">

    <div class="container container-lg mt-1 mb-md">
        <div class="row">
            <div class="col-12">
                <div class="text-dark font-weight-bold font-gotham-bold slider-hone-text name">Portofolio List</div>
            </div>
        </div>
        <div class="row no-gutters">
            @foreach($model as $key => $value)
            <div class="item">
                <a href="{{ $value->link }}" target="_blank">
                    <div class="box-creator">
                        <div class="position-relative">
                            <div class="overlay"></div>
                            <img src="{{ url($value->image) }}" class="card-img">
                        </div>

                    </div>
                </a>
            </div>
            @endforeach
        </div>
    </div>
</div>
@endsection