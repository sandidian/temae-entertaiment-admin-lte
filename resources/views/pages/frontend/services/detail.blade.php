@extends('layouts.master',['data' => ['navbarHome'=> false]])

@section('title','Services - '.$model->name)

@section('content')

<div class="wrapper mt-4" id="servicesDetail">

    <div class="container container-lg mt-1 mb-md">

        <div class="row mt-5">
            <div class="col-md-7">
                <div class="row">
                    <div class="col-12">
                        <div class="text-dark font-weight-bold name" >{{ $model->name }}
                        </div>
                        <p class="text-dark font-weight-light">{!! $model->description !!}</p>
                    </div>
                </div>
            </div>
            <div class="col-md-5">
                <img src="{{ url($model->image) }}" width="100%" class="rounded">
            </div>
        </div>
    </div>
</div>
@endsection