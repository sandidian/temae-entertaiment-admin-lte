@extends('layouts.master',['data' => ['navbarHome'=> true]])

@section('title', 'Services')

@section('css')
<style>

</style>
@endsection

@section('content')
<div class="wrapper" id="services">

    <div class="position-relative text-left" id="servicesBanner">
        <img src="{{ asset('assets/services/3.jpg') }}" width="100%">
        <div class="title">
            <div class="text-white font-weight-bold font-gotham-bold slider-hone-text">Lorem ipsum dolor amet.</div>
        </div>
    </div>


    <div class="container container-lg mt-2" id="services-list">
        <div class="row">
            <div class="col-md-12">
                <h3 class="font-gotham-bold title-section name">Services</h3>
            </div>
        </div>
        <div class="row" id="serviceList">

            @foreach($services as $key => $value)
            <div class="col-12 col-md-3">
                <a href="{{ route('frontend.services.detail',$value->slug) }}">
                    <img src="{{ url($value->image) }}" class="mx-auto d-block" width="100%">
                    <div class="h5 text-center text-dark">
                        {{ $value->name }}
                    </div>
                </a>
            </div>
            @endforeach

        </div>
    </div>

    <div class="container-fluid mt-5 px-0 position-relative" id="services-portofolio">

        <div class="name-container">
            <div class="font-weight-bold name">
                Portofolio
            </div>
            <div class="h4 action-desktop">
                <a href="{{ route('frontend.services.portofolio') }}">
                    View All
                </a>
            </div>
        </div>
        <div class="row no-gutters">
            @foreach($portofolio as $key => $value)
            <div class="item @if($key > 3) item-mobile @endif">
                <a href="{{ $value->link }}" target="_blank">
                    <div class="box-creator">
                        <div class="position-relative">
                            <div class="overlay"></div>
                            <img src="{{ url($value->image) }}" class="card-img">
                        </div>

                    </div>
                </a>
            </div>
            @endforeach
        </div>
        <div class="action-mobile">
            <a href="{{ route('frontend.services.portofolio') }}">
                <div class="font-weight-bold text-uppercase text-center text-dark mt-3">
                    <u> View All</u>
                </div>
            </a>
        </div>

    </div>


    <div class="container container-lg my-5" id="services-our-client">

        <div class="row name-mobile">
            <div class="col-md-12">
                <h3 class="font-gotham-bold title-section">Our Client</h3>
            </div>
        </div>

        <div class="row" id="ourClient">

            @foreach(arrOurClient() as $key => $value)

            <div class="p-2 item">
                <img src="{{ asset('assets/logo-client/'.$value.'') }}" width="100%;">
            </div>

            @endforeach

            <div class="p-2 name-desktop">
                <div class="display-2 text-uppercase text-center font-weight-bold">Our Client</div>
            </div>

        </div>
    </div>


</div>
@endsection

@section('js')
@endsection