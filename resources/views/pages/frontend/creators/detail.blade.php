@extends('layouts.master',['data' => ['navbarHome'=> true]])

@section('title', $model->name ." - Content Creator")

@section('css')
@endsection

@section('content')
<div class="wrapper" id="creator-detail">
    <section class="section-profile-cover section-shaped my-0">
        <!-- Circles background -->
        <img class="bg-image" src="{{ url($model->image_cover) }}" style="width: 100%;">
        <!-- SVG separator -->
    </section>
    <div class="container container-lg mt-1 mb-md">

        <div class="row mt-5">
            <div class="col-md-7">
                <div class="row">
                    <div class="col-12">
                        <div class="text-dark font-weight-bold name">{{ $model->name }}
                        </div>
                        <p class="text-dark font-weight-light">{{ $model->description }}</p>
                    </div>
                    <div class="col-12">
                        <div class="row">
                            @foreach(sosmed() as $k => $v)
                            <div class="col-6">
                                <div class="py-1">
                                    <a {{ $model->sosmed[$k]['link'] ? 'target="_blank"' : '' }}
                                        href="{{ $model->sosmed[$k]['link'] ? $model->sosmed[$k]['link'] : 'javascript:;'  }}">

                                        <h4 class="text-dark"><i class="fab fa-fw fa-{{$k}} " aria-hidden="true"></i>
                                            {{ $model->sosmed[$k]['count'] ? $model->sosmed[$k]['count'] : 0 }}</h4>
                                    </a>

                                </div>
                            </div>
                            @endforeach
                        </div>
                    </div>
                </div>
            </div>
            <div class="col-md-5">
                <img src="{{ url($model->image) }}" width="100%" class="rounded">
            </div>
        </div>
    </div>
</div>
@endsection

@section('js')
@endsection