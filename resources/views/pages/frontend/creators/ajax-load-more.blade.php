@foreach($creators as $key => $model)

@php
$sosmed = json_decode($model->sosmed, true);
@endphp

<div class="col-12 col-md-4 creator-item">
    <div class="box-creator ">
        <div class="position-relative">
            <img src="{{ url($model->image) }}" class="card-img">
            <div class="box-name text-center">
                <div class="font-weight-bold display-4 text-white">{{$model->name}}</div>
            </div>
            <div class="box-sosmed pt-3 text-white">
                <div class="d-flex flex-row justify-content-center">

                    @foreach(sosmed() as $k => $v)

                    <div class="d-flex flex-row px-2 py-3 text-white">
                        <i class="fab fa-fw fa-lg fa-{{ $k }} logo"></i>
                        <div class="font-weight-thin">
                            {{ $sosmed[$k]['count'] ? $sosmed[$k]['count'] : 0 }}</div>
                    </div>
                    @endforeach
                </div>
                <a href="{{ route('frontend.creators.detail',$model->slug) }}">
                    <div class="text-white text-center bg-yellow py-2">See Profile</div>
                </a>
            </div>
        </div>

    </div>
</div>

@endforeach