@extends('layouts.master',['data' => ['navbarHome'=> true]])

@section('title','Creators')

@section('css')
@endsection

@section('content')
<div class="wrapper" id="creators">
    <div class="position-relative text-left" id="creatorsBanner">
        <img src="{{ asset('assets/services/4.jpg') }}" width="100%">
        <div class="title">
            <div class="text-white font-weight-bold font-gotham-bold slider-hone-text text-center">Meet Our</div>
            <div class="text-white font-weight-bold font-gotham-bold slider-hone-text text-center">Creators</div>
        </div>
    </div>

    <div class="container container-lg" id="creator-data">

        <div class="row" id="creatorList">

            @foreach($creators as $key => $model)

            @php
            $sosmed = json_decode($model->sosmed, true);
            @endphp

            <div class="col-12 col-md-4 creator-item">
                <div class="box-creator ">
                    <div class="position-relative">
                        <img src="{{ url($model->image) }}" class="card-img">
                        <div class="box-name text-center">
                            <div class="font-weight-bold display-4 text-white">{{$model->name}}</div>
                        </div>
                        <div class="box-sosmed pt-3 text-white">
                            <div class="d-flex flex-row justify-content-center">

                                @foreach(sosmed() as $k => $v)

                                <div class="d-flex flex-row px-2 py-3 text-white">
                                    <i class="fab fa-fw fa-lg fa-{{ $k }} logo"></i>
                                    <div class="font-weight-thin">
                                        {{ $sosmed[$k]['count'] ? $sosmed[$k]['count'] : 0 }}</div>
                                </div>
                                @endforeach
                            </div>
                            <a href="{{ route('frontend.creators.detail',$model->slug) }}">
                                <div class="text-white text-center bg-yellow py-2">See Profile</div>
                            </a>
                        </div>
                    </div>

                </div>
            </div>

            @endforeach

        </div>

        @if(App\Creators::where(['status' => 1])->count() >= 6)

        <div class="row justify-content-md-center py-3">
            <div class="col-12 col-md-3 ">
                <div class="h4 py-2 col-12 load-more text-center text-dark"
                    data-totalResult="{{ App\Creators::where(['status' => 1])->count() }}" style="cursor:pointer;">
                    Load More</div>
            </div>
        </div>

        @endif

    </div>

</div>
@endsection

@section('js')
<script type="text/javascript">
$(document).ready(function() {
    $(".load-more").on('click', function() {
        var _totalCurrentResult = $(".box-creator").length;
        // Ajax Reuqest
        $.ajax({
            url: "{{ route('frontend.creators.load-more-data') }}",
            type: 'get',
            dataType: 'json',
            data: {
                skip: _totalCurrentResult
            },
            beforeSend: function() {
                $(".load-more").html('Loading...');
            },
            success: function(response) {

                $("#creatorList").append(response.view);
                var _totalCurrentResult = $(".box-creator").length;
                var _totalResult = parseInt($(".load-more").attr(
                    'data-totalResult'));

                if (_totalCurrentResult == _totalResult) {
                    $(".load-more").remove();
                } else {
                    $(".load-more").html('Load More');
                }
            }
        });
    });
});
</script>
@endsection