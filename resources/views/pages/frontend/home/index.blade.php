@extends('layouts.master',['data' => ['navbarHome'=> true]])

@section('title','Home')

@section('css')
<link rel="stylesheet" href="{{ asset('vendor/owl-carousel/assets/owl.carousel.min.css') }}" />
<link rel="stylesheet" href="{{ asset('vendor/owl-carousel/assets/owl.theme.default.min.css') }}" />
@endsection

@section('content')
<div class="wrapper">

    <div class="owl-carousel owl-theme " id="sliderHome">

        @foreach($slider_home as $key => $value)

        <div class="position-relative text-left">
            <img width="100%" src="{{ url($value->image) }}">
            <div class="title-carousel">
                <div class="text-white font-weight-bold font-gotham-bold slider-hone-text">{{ $value->name }}</div>
            </div>
        </div>

        @endforeach

    </div>
    <div id="home-news">
        <div class="container container-lg">
            <div class="row">

                <div class="col-12 col-md-4 title-mobile">
                    <div class="text-center text-dark name-top">
                        What's
                    </div>
                    <div class="text-center text-dark name-bottom">
                        News?
                    </div>
                </div>

                @if($news[0])
                <div class="col-12 col-md-4">
                    <a href="{{ route('frontend.videos.detail',$news[0]->slug) }}">
                        <img width="100%"
                            src="https://img.youtube.com/vi/{{ youtubeID($news[0]->youtube) }}/mqdefault.jpg"
                            alt="First slide" style="object-fit: cover;" class="rounded">
                        <div class="title p-3 text-dark text-center">
                            {{$news[0]->title}}
                        </div>
                    </a>
                </div>
                @endif
                <div class="col-12 col-md-4 title-desktop">
                    <div class="text-center text-dark name-top">
                        What's
                    </div>
                    <div class="text-center text-dark name-bottom">
                        News?
                    </div>
                    <a href="{{ route('frontend.videos') }}">
                        <div class="font-weight-bold text-uppercase text-center text-dark mt-n3 pb-4">
                            <u> View All</u>
                        </div>
                    </a>
                </div>
                @if($news[1])
                <div class="col-12 col-md-4">
                    <a href="{{ route('frontend.videos.detail',$news[1]->slug) }}">
                        <a href="{{ route('frontend.videos.detail',$news[1]->slug) }}">
                            <img width="100%"
                                src="https://img.youtube.com/vi/{{ youtubeID($news[1]->youtube) }}/mqdefault.jpg"
                                alt="First slide" style="object-fit: cover;" class="rounded">

                            <div class="title p-3 text-dark text-center">
                                {{$news[1]->title}}
                            </div>
                        </a>

                </div>
                @endif
                <div class="col-12 mt-4 view-all-mobile">
                    <a href="{{ route('frontend.videos') }}">
                        <div class="font-weight-bold text-uppercase text-center text-dark mt-n3 pb-4">
                            <u> View All</u>
                        </div>
                    </a>
                </div>

            </div>
        </div>
    </div>

    <div class="container-fluid mt-5" id="home-creators">
        <div class="row">
            <div class="col-12">
                <div class="">
                    <div class="action-container">

                        <div class="row justify-content-center text-center text-white m-0">

                            <div class="col-12 col-md-3 ">
                                <div class="display-1 name-top">
                                    Content
                                </div>
                                <div class="display-1 name-bottom">
                                    Creators
                                </div>
                                <div class="text-white mt-4 description">
                                    Lorem ipsum dolor sit, amet consectetur adipisicing elit. Quae voluptates soluta
                                    inventore culpa deleniti quidem.</div>
                                <a class="btn btn-secondary btn-sm mt-3 text-capitalize"
                                    href="{{ route('frontend.creators') }}">
                                    View All
                                </a>
                            </div>
                        </div>
                    </div>

                    <div class="main-content">
                        <div class="owl-carousel owl-theme" id="creatorList">

                            @foreach($creators as $key => $model)

                            @php
                            $sosmed = json_decode($model->sosmed, true);
                            @endphp

                            <div class="item">

                                <div class="box-creator">

                                    <div class="position-relative">
                                        <div class="overlay"></div>
                                        <img src="{{ url($model->image) }}" class="card-img">

                                        <div class="box-sosmed text-white">
                                            <div class="d-flex flex-row justify-content-center">

                                                @foreach(sosmed() as $k => $v)

                                                <div class="d-flex flex-row p-2">
                                                    <i class="fab fa-fw fa-lg fa-{{ $k }} logo"></i>
                                                    <div class="font-weight-thin">
                                                        {{ $sosmed[$k]['count'] ? $sosmed[$k]['count'] : 0 }}</div>
                                                </div>
                                                @endforeach
                                            </div>
                                        </div>
                                    </div>
                                </div>

                            </div>

                            @endforeach

                        </div>

                    </div>

                </div>
            </div>
        </div>


    </div>

</div>
</section>
@endsection

@section('js')

<script src="{{ asset('vendor/owl-carousel/owl.carousel.min.js') }}"></script>

<script>
$(document).ready(function($) {
    $('#sliderHome').owlCarousel({
        dots: true,
        items: 1,
        animateOut: 'fadeOut',
        loop: true,
        autoplay: true,
        autoplayTimeout: 5000,
        autoplayHoverPause: true,
    });

    $("#creatorList").owlCarousel({
        items: 3,
        loop: true,
        margin: 20,
        responsiveClass: true,
        responsive: {
            300: {
                items: 1,
                dots: false
            },
            600: {
                items: 1,
                dots: false
            },
            1000: {
                items: 3,
                dots: false
            }
        }
    });

});
</script>
@endsection