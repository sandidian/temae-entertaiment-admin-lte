<div class="form-group">
    <label for="ourAffiliatesImage1">Image 1</label>

    <div class="input-group">
        <div class="custom-file">
            <input type="file" class="custom-file-input" onChange="showImage(this,'#previewImageOurAffiliates1')"
                accept="image/*" id="ourAffiliatesImage1">
            <label class="custom-file-label">Choose file</label>
        </div>
        <div class="input-group-append">
            <button class="btn btn-outline-success add_button" type="button"><span class="fa fa-plus fa-lg"
                    aria-hidden="true"></span></button>
        </div>
    </div>

    <img style="width:10rem" class="mt-2 d-none" id="previewImageOurAffiliates1">
</div>