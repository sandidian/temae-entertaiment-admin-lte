@extends('adminlte::page')

@section('title', 'Edit About Us')

@section('content_header')
<h1 class="m-0 text-dark">Edit About Us</h1>
@stop

@push('css')
<style>
.note-group-select-from-files {
    display: none !important;
}
</style>
@endpush

@section('content')

<form action="{{ route('about.update') }}" method="post" enctype="multipart/form-data">
    @csrf
    <div class="row">
        <div class="col-12 ">
            <div class="card @error('about.who_we_are.description') card-danger @enderror">
                <div class="card-header">
                    Who We Are
                </div>
                <div class="card-body">

                    <div class="form-group">
                        <label for="whoWeAreDescription">Description</label>
                        <textarea
                            class="summernote form-control @error('about.who_we_are.description') is-invalid @enderror"
                            id="whoWeAreDescription" placeholder="Description"
                            name="about[who_we_are][description]">{{ $about['who_we_are']['description'] }}</textarea>
                        @error('about.who_we_are.description') <span class="text-danger">{{$message}}</span> @enderror
                    </div>

                    <div class="form-group">
                        <label for="whoWeAreImage">Image</label>

                        <div class="custom-file">
                            <input type="file" class="custom-file-input" id="whoWeAreImage" name="whoWeAreImage"
                                placeholder="Image" onChange="showImage(this,'#previewImageWhoWeAre')" accept="image/*">
                            <label class=" custom-file-label" for="whoWeAreImage">Choose file</label>
                            <input type="hidden" name="about[who_we_are][image]"
                                value="{{ $about['who_we_are']['image'] }}">
                        </div>

                        <img src="{{ url($about['who_we_are']['image']) }}" alt="" style="width:20rem" class="mt-4"
                            id="previewImageWhoWeAre">

                    </div>

                </div>

            </div>
        </div>
        <div class="col-md-12">
            <div class="card">
                <div class="card-header">
                    Our Achievements
                </div>
                <div class="card-body">
                    <div class="form-group">
                        <label for="ourAchievementsImage">Image</label>

                        <div class="custom-file">
                            <input type="file" class="custom-file-input" id="ourAchievementsImage"
                                name="ourAchievementsImage" placeholder="Image"
                                onChange="showImage(this,'#previewImageOurAchievements')" accept="image/*">
                            <label class=" custom-file-label" for="ourAchievementsImage">Choose file</label>
                            <input type="hidden" name="about[our_achievements][image]"
                                value="{{ $about['our_achievements']['image'] }}">
                        </div>

                        <img src="{{ url($about['our_achievements']['image']) }}" alt="" style="width:20rem"
                            class="mt-4" id="previewImageOurAchievements">

                    </div>

                </div>

            </div>
        </div>
        <div class="col-12 ">
            <div class="card @error('about.core_business.description') card-danger @enderror">
                <div class="card-header">
                    Core Business
                </div>
                <div class="card-body">

                    <div class="form-group">
                        <label for="coreBusinessDescription">Description</label>
                        <textarea class="summernote form-control " id="coreBusinessDescription"
                            placeholder="Description"
                            name="about[core_business][description]">{{ $about['core_business']['description'] }}</textarea>
                        @error('about.core_business.description') <span class="text-danger">{{$message}}</span>
                        @enderror
                    </div>

                    <div class="form-group">
                        <label for="coreBusinessImage">Image</label>

                        <div class="custom-file">
                            <input type="file" class="custom-file-input" id="coreBusinessImage" name="coreBusinessImage"
                                placeholder="Image" onChange="showImage(this,'#previewImageCoreBusiness')"
                                accept="image/*">
                            <label class=" custom-file-label" for="coreBusinessImage">Choose file</label>
                            <input type="hidden" name="about[core_business][image]"
                                value="{{ $about['core_business']['image'] }}">
                        </div>

                        <img src="{{ url($about['core_business']['image']) }}" alt="" style="width:20rem" class="mt-4"
                            id="previewImageCoreBusiness">

                    </div>

                </div>

            </div>
        </div>
        <div class="col-md-12 d-none">
            <div class="card">
                <div class="card-header">
                    Our Affiliates
                </div>
                <div class="card-body field_wrapper">

                    @foreach($about['our_affiliates'] as $key => $value)

                    <div class="form-group our_affiliates" id="data-our-affiliates-{{$key}}">
                        <label for="ourAffiliatesImage{{$key+1}}">Image {{$key+1}}</label>

                        <div class="input-group">
                            <div class="custom-file">
                                <input type="file" class="custom-file-input"
                                    onChange="showImage(this,'#previewImageOurAffiliates{{$key+1}}')" accept="image/*"
                                    id="ourAffiliatesImage{{$key+1}}" accept="image/*">

                                <input type="hidden" name="about[our_affiliates][{{$key}}]"
                                    value="{{ $about['our_affiliates'][$key] }}">
                                <label class="custom-file-label">Choose file</label>
                            </div>
                            <div class="input-group-append">
                                <button
                                    class="btn {{ $key > 0 ? 'btn-outline-danger' : 'btn-outline-success' }} {{ $key > 0 ? 'remove_button' : 'add_button' }}"
                                    {{ $key > 0 ? "onClick=remove('#data-our-affiliates-".$key."')" : '' }}
                                    type="button"><span class="fa {{ $key > 0 ? 'fa-trash' : 'fa-plus' }} fa-lg"
                                        aria-hidden="true"></span></button>
                            </div>
                        </div>

                        <img src="{{ url($value) }}" style="width:10rem" class="mt-2"
                            id="previewImageOurAffiliates{{$key+1}}">
                    </div>

                    @endforeach
                </div>

            </div>
        </div>
        <div class="col-md-12">
            <button type="submit" class="btn btn-primary">Update</button>
        </div>
    </div>
</form>
@stop

@push('js')

<script>
function remove(id_input) {
    $(id_input).remove();
}

function showImage(id_input, id_image) {

    if (id_input.files && id_input.files[0]) {
        var reader = new FileReader();
        reader.onload = function(e) {
            $(id_image).attr('src', e.target.result);
            $(id_image).removeClass("d-none")
        }
        reader.readAsDataURL(id_input.files[0]);
    } else {
        alert('select a file to see preview');
        $(id_image).attr('src', '');
    }
}

$(".custom-file-input").on("change", function() {
    var fileName = $(this).val().split("\\").pop();
    $(this).siblings(".custom-file-label").addClass("selected").html(fileName);
});

$('.summernote').summernote();
</script>

<script type="text/javascript">
$(document).ready(function() {
    var maxField = 10; //Input fields increment limitation
    var addButton = $('.add_button'); //Add button selector
    var wrapper = $('.field_wrapper'); //Input field wrapper
    var total = $(".our_affiliates").length

    var x = 1; //Initial field counter is 1

    //Once add button is clicked
    $(addButton).click(function() {
        //Check maximum number of input fields
        if (x < maxField) {
            alert(total);
        }
    });

    //Once remove button is clicked
    // $(wrapper).on('click', '.remove_button', function(e) {
    //     e.preventDefault();
    //     $(this).parent('div').remove(); //Remove field html
    //     x--; //Decrement field counter
    // });
});
</script>
@endpush