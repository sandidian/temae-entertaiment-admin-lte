@extends('adminlte::page')

@section('title', 'About Us - Edit Core Business')

@section('content_header')
<h1 class="m-0 text-dark">About Us - Edit Core Business</h1>
@stop

@push('css')
<style>
.note-group-select-from-files {
    display: none !important;
}
</style>
@endpush

@section('content')

<form action="{{ route('core-business.update') }}" method="post" enctype="multipart/form-data">
    @csrf
    <div class="row">
        <div class="col-12 ">
            <div class="card @error('input.description') card-danger @enderror">
                <div class="card-header">
                    Core Business
                </div>
                <div class="card-body">

                    <div class="form-group">
                        <label for="Description">Description</label>
                        <textarea class="summernote form-control @error('input.description') is-invalid @enderror"
                            id="Description" placeholder="Description"
                            name="input[description]">{{ $about['core_business']['description'] }}</textarea>
                        @error('input.description') <span class="text-danger">{{$message}}</span> @enderror
                    </div>

                    <div class="form-group">
                        <label for="Image">Image</label>

                        <div class="custom-file">
                            <input type="file" class="custom-file-input" id="Image" name="image" placeholder="Image"
                                onChange="showImage(this,'#previewImage')" accept="image/*">
                            <label class=" custom-file-label" for="Image">Choose file</label>
                            <input type="hidden" name="input[image]" value="{{ $about['core_business']['image'] }}">
                        </div>

                        <img src="{{ url($about['core_business']['image']) }}" alt="" style="width:20rem" class="mt-4"
                            id="previewImage">

                    </div>

                </div>
                <div class="card-footer">
                    <button type="submit" class="btn btn-primary">Update</button>
                </div>

            </div>
        </div>
    </div>
</form>
@stop

@push('js')

<script>
function remove(id_input) {
    $(id_input).remove();
}

function showImage(id_input, id_image) {

    if (id_input.files && id_input.files[0]) {
        var reader = new FileReader();
        reader.onload = function(e) {
            $(id_image).attr('src', e.target.result);
            $(id_image).removeClass("d-none")
        }
        reader.readAsDataURL(id_input.files[0]);
    } else {
        alert('select a file to see preview');
        $(id_image).attr('src', '');
    }
}

$(".custom-file-input").on("change", function() {
    var fileName = $(this).val().split("\\").pop();
    $(this).siblings(".custom-file-label").addClass("selected").html(fileName);
});

$('.summernote').summernote();
</script>

<script type="text/javascript">
$(document).ready(function() {
    var maxField = 10; //Input fields increment limitation
    var addButton = $('.add_button'); //Add button selector
    var wrapper = $('.field_wrapper'); //Input field wrapper
    var total = $(".our_affiliates").length

    var x = 1; //Initial field counter is 1

    //Once add button is clicked
    $(addButton).click(function() {
        //Check maximum number of input fields
        if (x < maxField) {
            alert(total);
        }
    });

    //Once remove button is clicked
    // $(wrapper).on('click', '.remove_button', function(e) {
    //     e.preventDefault();
    //     $(this).parent('div').remove(); //Remove field html
    //     x--; //Decrement field counter
    // });
});
</script>
@endpush