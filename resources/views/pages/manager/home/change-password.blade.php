@extends('adminlte::page')

@section('title', 'Change Password')

@section('content_header')
<h1 class="m-0 text-dark">Change Password</h1>
@stop

@section('content')
<form action="{{route('home.change-password.update')}}" method="post">
    @csrf
    <div class="row">
        <div class="col-12">
            <div class="card">
                <div class="card-body">

                    <div class="form-group">
                        <label for="InputOldPassword">Old Password</label>
                        <input type="password" class="form-control @error('password') is-invalid @enderror"
                            id="InputOldPassword" placeholder="Old Password" name="password">
                        @error('password') <span class="text-danger">{{$message}}</span> @enderror
                    </div>
                    <div class="form-group">
                        <label for="InputNewPassword">New Password</label>
                        <input type="password" class="form-control" id="InputNewPassword" placeholder="New Password"
                            name="new_password">
                        @error('new_password') <span class="text-danger">{{$message}}</span> @enderror
                    </div>

                </div>

                <div class="card-footer">
                    <button type="submit" class="btn btn-primary">Update</button>
                    <a href="{{route('home')}}" class="btn btn-default">
                        Cancel
                    </a>
                </div>
            </div>
        </div>
    </div>
    @stop