@extends('adminlte::page')

@section('title', 'Edit Profile')

@section('content_header')
<h1 class="m-0 text-dark">Edit Profile</h1>
@stop

@section('content')
<form action="{{route('home.profile.update')}}" method="post">
    @csrf
    <div class="row">
        <div class="col-12">
            <div class="card">
                <div class="card-body">


                    <div class="form-group">
                        <label for="inputName">Name</label>
                        <input type="text" class="form-control @error('name') is-invalid @enderror"
                            id="inputName" placeholder="Name" name="name"
                            value="{{$model->name ?? old('name')}}">
                        @error('name') <span class="text-danger">{{$message}}</span> @enderror
                    </div>

                    <div class="form-group">
                        <label for="inputEmail">Email</label>
                        <input type="email" class="form-control @error('email') is-invalid @enderror"
                            id="inputEmail" placeholder="Email" name="email"
                            value="{{$model->email ?? old('email')}}">
                        @error('email') <span class="text-danger">{{$message}}</span> @enderror
                    </div>

                </div>

                <div class="card-footer">
                    <button type="submit" class="btn btn-primary">Update</button>
                    <a href="{{route('home')}}" class="btn btn-default">
                        Cancel
                    </a>
                </div>
            </div>
        </div>
    </div>
    @stop