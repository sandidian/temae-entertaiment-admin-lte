@extends('adminlte::page')

@section('title', 'Add Our Client')

@section('content_header')
<h1 class="m-0 text-dark">Add Our Client</h1>
@stop

@push('css')
<style>
.video-container {
    position: relative;
    padding-bottom: 56.25%;
    padding-top: 30px;
    height: 0;
    overflow: hidden;
}

.video-container iframe,
.video-container object,
.video-container embed {
    position: absolute;
    top: 0;
    left: 0;
    width: 100%;
    height: 100%;
}

.note-group-select-from-files {
    display: none;
}
</style>
@endpush

@section('content')

<form action="{{route('our-client.store')}}" method="post" enctype="multipart/form-data">
    @csrf
    <div class="row">
        <div class="col-12">
            <div class="card">
                <div class="card-body">

                    <div class="form-group">
                        <label for="InputName">Name</label>
                        <input type="text" class="form-control @error('name') is-invalid @enderror" id="InputName"
                            placeholder="Name" name="name" value="{{old('name')}}">
                        @error('name') <span class="text-danger">{{$message}}</span> @enderror
                    </div>

                    <div class="form-group">
                        <label for="InputYoutube">Youtube (URL)</label>

                        <div class="input-group">
                            <input type="text" class="form-control @error('link') is-invalid @enderror"
                                id="InputYoutube" placeholder="Youtube" name="link" value="{{old('link')}}">
                            <div class="input-group-append">
                                <button class="btn btn-success" type="button" id="checkYoutube">Show</button>
                            </div>
                        </div>

                        @error('link') <span class="text-danger d-flex">{{$message}}</span> @enderror

                        <div class="{{ old('link') ? 'd-flex' : '' }} flex-column d-none" id="previewHolderBox">
                            <div class="font-weight-bold mt-1">Thumbnail :</div>

                            <div class="row">
                                <div class="col-12 col-md-5">
                                    <img class="img-fluid mt-2" id="previewHolder"
                                        src="{{ old('link') ? 'https://img.youtube.com/vi/'.youtubeID(old('link')).'/mqdefault.jpg' : '' }}">
                                </div>
                            </div>
                        </div>

                    </div>


                    <div class="form-group">
                        <label for="InputStatus">Status</label>
                        <select name="status" id="InputStatus" class="form-control">
                            @foreach(statusNews() as $key => $value)
                            <option value="{{ $key }}">{{ $value }}
                            </option>
                            @endforeach
                        </select>
                    </div>

                </div>

                <div class="card-footer">
                    <button type="submit" class="btn btn-primary">Simpan</button>
                    <a href="{{route('our-client.index')}}" class="btn btn-default">
                        Batal
                    </a>
                </div>
            </div>
        </div>
    </div>
    @stop

    @push('js')
    <script type="text/javascript">
    function YouTubeGetID(url) {
        var ID = '';
        url = url.replace(/(>|<)/gi, '').split(/(vi\/|v=|\/v\/|youtu\.be\/|\/embed\/)/);
        if (url[2] !== undefined) {
            ID = url[2].split(/[^0-9a-z_\-]/i);
            ID = ID[0];
        } else {
            ID = url;
        }
        return ID;
    }

    $(document).ready(function() {

        if ($("#InputYoutube").val() == "") {
            $("#checkYoutube").attr('disabled', true);
        } else {
            $("#checkYoutube").attr('disabled', false);
        }

        $("#InputYoutube").keyup(function() {

            var input = $(this);

            if (input.val() == "") {
                $("#checkYoutube").attr('disabled', true);
                $('#previewHolderBox').removeClass("d-flex")
                $('#previewHolderBox').addClass("d-none")
            } else {
                $("#checkYoutube").attr('disabled', false);
            }

        })

        $("#checkYoutube").click(function() {
            const input = $("#InputYoutube").val()

            const id = YouTubeGetID(input)

            const url = 'https://img.youtube.com/vi/' + id + '/mqdefault.jpg';

            $('#previewHolder').attr('src', url);
            $('#previewHolderBox').removeClass("d-none")
            $('#previewHolderBox').addClass("d-flex")

        })

    });
    </script>

    @endpush