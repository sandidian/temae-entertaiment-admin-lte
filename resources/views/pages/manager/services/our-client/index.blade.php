@extends('adminlte::page')

@section('title', 'List Our Client')

@section('content_header')
<h1 class="m-0 text-dark">List Our Client</h1>
@stop

@section('content')

<div class="row">
    <div class="col-12">
        <div class="card">
            <div class="card-body">

                <a href="{{route('our-client.create')}}" class="btn btn-primary mb-2">
                    Add
                </a>

                <table class="table table-hover table-bordered table-stripped" id="example2">
                    <thead>
                        <tr>
                            <th>No.</th>
                            <th>Name</th>
                            <th>Thumbnail</th>
                            <th>Status</th>
                            <th>Option</th>
                        </tr>
                    </thead>
                    <tbody>
                        @foreach($models as $key => $model)
                        <tr>
                            <td>{{$key+1}}</td>
                            <td>
                                {{$model->name}}
                            </td>

                            <td>
                                <img src="https://img.youtube.com/vi/{{ youtubeID($model->link) }}/mqdefault.jpg"
                                    alt="" width="150">
                            </td>

                            <td>{{ statusNews($model->status) }}</td>
                            <td>
                                <a href="{{route('our-client.edit', $model->id)}}" class="btn btn-primary btn-xs">
                                    Edit
                                </a>
                                <a href="{{route('our-client.destroy', $model->id)}}"
                                    onclick="notificationBeforeDelete(event, this)" class="btn btn-danger btn-xs">
                                    Delete
                                </a>
                            </td>
                        </tr>
                        @endforeach
                    </tbody>
                </table>

            </div>
        </div>
    </div>
</div>
@stop

@push('js')
<form action="" id="delete-form" method="post">
    @method('delete')
    @csrf
</form>
<script>
$('#example2').DataTable({
    "responsive": true,
});

function notificationBeforeDelete(event, el) {
    event.preventDefault();
    if (confirm('Are you sure you will delete the data? ')) {
        $("#delete-form").attr('action', $(el).attr('href'));
        $("#delete-form").submit();
    }
}
</script>
@endpush