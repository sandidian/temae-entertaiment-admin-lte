@extends('adminlte::page')

@section('title', 'Add Services')

@section('content_header')
<h1 class="m-0 text-dark">Add Services</h1>
@stop

@push('css')
<style>
.video-container {
    position: relative;
    padding-bottom: 56.25%;
    padding-top: 30px;
    height: 0;
    overflow: hidden;
}

.video-container iframe,
.video-container object,
.video-container embed {
    position: absolute;
    top: 0;
    left: 0;
    width: 100%;
    height: 100%;
}

.note-group-select-from-files {
    display: none;
}
</style>
@endpush

@section('content')

<form action="{{route('services.store')}}" method="post" enctype="multipart/form-data">
    @csrf
    <div class="row">
        <div class="col-12">
            <div class="card">
                <div class="card-body">

                    <div class="form-group">
                        <label for="InputName">Name</label>
                        <input type="text" class="form-control @error('name') is-invalid @enderror" id="InputName"
                            placeholder="Name" name="name" value="{{old('name')}}">
                        @error('name') <span class="text-danger">{{$message}}</span> @enderror
                    </div>

                    <div class="form-group">
                        <label for="InputImage">Image</label>

                        <div class="custom-file">
                            <input type="file" class="custom-file-input @error('image') is-invalid @enderror"
                                id="InputImage" name="image" placeholder="Image"
                                onChange="showImage(this,'#previewImage')" accept="image/*">
                            <label class=" custom-file-label" for="InputImage">Choose file</label>
                        </div>

                        @error('image') <span class="text-danger d-flex">{{$message}}</span> @enderror

                        <img src="" alt="" width="150" class="mt-4 d-none" id="previewImage">

                    </div>

                    <div class="form-group">
                        <label for="InputDescription">Description</label>

                        <textarea class="summernote @error('description') is-invalid @enderror" id="InputDescription"
                            name="description">{{old('description')}}</textarea>
                        @error('description') <span class="text-danger">{{$message}}</span> @enderror

                    </div>


                    <div class="form-group">
                        <label for="InputStatus">Status</label>
                        <select name="status" id="InputStatus" class="form-control">
                            @foreach(statusNews() as $key => $value)
                            <option value="{{ $key }}">{{ $value }}
                            </option>
                            @endforeach
                        </select>
                    </div>

                </div>

                <div class="card-footer">
                    <button type="submit" class="btn btn-primary">Simpan</button>
                    <a href="{{route('services.index')}}" class="btn btn-default">
                        Batal
                    </a>
                </div>
            </div>
        </div>
    </div>
    @stop

    @push('js')

    <script>
    function showImage(id_input, id_image) {

        if (id_input.files && id_input.files[0]) {
            var reader = new FileReader();
            reader.onload = function(e) {
                $(id_image).attr('src', e.target.result);
                $(id_image).removeClass("d-none")
            }
            reader.readAsDataURL(id_input.files[0]);
        } else {
            alert('select a file to see preview');
            $(id_image).attr('src', '');
        }
    }

    $(".custom-file-input").on("change", function() {
        var fileName = $(this).val().split("\\").pop();
        $(this).siblings(".custom-file-label").addClass("selected").html(fileName);
    });

    $('.summernote').summernote();
    </script>

    @endpush