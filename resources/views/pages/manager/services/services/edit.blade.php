@extends('adminlte::page')

@section('title', 'Edit Service')

@section('content_header')
<h1 class="m-0 text-dark">Edit Service</h1>
@stop

@section('content')

<form action="{{route('services.update', $model)}}" method="post" enctype="multipart/form-data">
    @method('PUT')
    @csrf
    <div class="row">
        <div class="col-12">
            <div class="card">
                <div class="card-body">

                    <div class="form-group">
                        <label for="InputName">Name</label>
                        <input type="text" class="form-control @error('name') is-invalid @enderror" id="InputName"
                            placeholder="Name" name="name" value="{{$model->name ?? old('name')}}">
                        @error('name') <span class="text-danger">{{$message}}</span> @enderror
                    </div>

                    <div class="form-group">
                        <label for="customFile">Image</label>

                        <div class="custom-file">
                            <input type="file" class="custom-file-input @error('image') is-invalid @enderror"
                                id="customFile" name="image" placeholder="Image"
                                onChange="showImage(this,'#previewImage')" accept="image/*">
                            <label class=" custom-file-label" for="customFile">Choose file</label>
                        </div>

                        @error('image') <span class="text-danger d-flex">{{$message}}</span> @enderror

                        <img src="{{ url($model->image) }}" alt="" width="150" class="mt-4" id="previewImage">

                    </div>

                    <div class="form-group">
                        <label for="InputContent">Content</label>

                    </div>

                    <div class="form-group">
                        <label for="InputDescription">Description</label>

                        <textarea class="summernote" id="InputDescription"
                            name="description">{{$model->description ?? old('description')}}</textarea>

                        @error('description') <span class="text-danger">{{$message}}</span> @enderror

                    </div>

                    <div class="form-group">
                        <label for="InputStatus">Status</label>
                        <select name="status" id="InputStatus" class="form-control">
                            @foreach(statusNews() as $key => $value)
                            <option value="{{ $key }}" {{ $model->status == $key? "selected" : "" }}>{{ $value }}
                            </option>
                            @endforeach
                        </select>
                    </div>

                </div>

                <div class="card-footer">
                    <button type="submit" class="btn btn-primary">Save</button>
                    <a href="{{route('services.index')}}" class="btn btn-default">
                        Cancel
                    </a>
                </div>
            </div>
        </div>
    </div>
    @stop

    @push('js')
    <script type="text/javascript">
    function showImage(id_input, id_image) {

        if (id_input.files && id_input.files[0]) {
            var reader = new FileReader();
            reader.onload = function(e) {
                $(id_image).attr('src', e.target.result);
                $(id_image).removeClass("d-none")
            }
            reader.readAsDataURL(id_input.files[0]);
        } else {
            alert('select a file to see preview');
            $(id_image).attr('src', '');
        }
    }

    $('.summernote').summernote();
    </script>

    @endpush