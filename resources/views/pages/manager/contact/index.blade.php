@extends('adminlte::page')

@section('title', 'Contact Us')

@section('content_header')
<h1 class="m-0 text-dark">Contact Us</h1>
@stop

@section('content')

<form action="{{ route('contact.update') }}" method="post">
    @csrf
    <div class="row">
        <div class="col-12 ">
            <div class="card @error('input.description') card-danger @enderror">
                <div class="card-header">
                    Contact Us
                </div>
                <div class="card-body">
                    <div class="form-group">
                        <label for="InputName">Name</label>

                        <input type="text" class="form-control @error('name') is-invalid @enderror" id="InputName"
                            placeholder="Name" name="name" value="{{$model->name ?? old('name')}}">
                        @error('name') <span class="text-danger">{{$message}}</span> @enderror
                    </div>

                    <div class="form-group">
                        <label for="InputLocation">Location</label>

                        <input type="text" class="form-control @error('input.location') is-invalid @enderror"
                            id="InputLocation" name="input[location]"
                            value="{{$contact['location']}}">
                        @error('input.location') <span class="text-danger">{{$message}}</span> @enderror
                    </div>

                    <div class="form-group">
                        <label for="InputEmail">Phone</label>

                        <input type="text" class="form-control @error('input.phone') is-invalid @enderror"
                            id="InputPhone" name="input[phone]"
                            value="{{$contact['phone']}}">
                        @error('input.phone') <span class="text-danger">{{$message}}</span> @enderror
                    </div>

                    <div class="form-group">
                        <label for="InputEmail">Email</label>

                        <input type="email" class="form-control @error('input.email') is-invalid @enderror"
                            id="InputEmail" name="input[email]"
                            value="{{$contact['email']}}">
                        @error('input.email') <span class="text-danger">{{$message}}</span> @enderror
                    </div>

                </div>
                <div class="card-footer">
                    <button type="submit" class="btn btn-primary">Update</button>
                </div>

            </div>
        </div>
    </div>
</form>
@stop