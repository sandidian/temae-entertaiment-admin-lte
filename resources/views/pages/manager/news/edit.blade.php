@extends('adminlte::page')

@section('title', 'Edit Slider Home')

@section('content_header')
<h1 class="m-0 text-dark">Edit Slider Home</h1>
@stop

@section('content')

<form action="{{route('news.update', $model)}}" method="post" enctype="multipart/form-data">
    @method('PUT')
    @csrf
    <div class="row">
        <div class="col-12">
            <div class="card">
                <div class="card-body">


                    <div class="form-group">
                        <label for="InputTitle">Title</label>
                        <input type="text" class="form-control @error('title') is-invalid @enderror" id="InputTitle"
                            placeholder="Title" name="title" value="{{$model->title ?? old('title')}}">
                        @error('title') <span class="text-danger">{{$message}}</span> @enderror
                    </div>

                    <div class="form-group">
                        <label for="InputYoutube">Youtube (URL)</label>

                        <div class="input-group">
                            <input type="text" class="form-control @error('youtube') is-invalid @enderror"
                                id="InputYoutube" placeholder="Youtube" name="youtube"
                                value="{{$model->youtube ?? old('youtube')}}">
                            <div class="input-group-append">
                                <button class="btn btn-success" type="button" id="checkYoutube">Show</button>
                            </div>
                        </div>

                        @error('youtube') <span class="text-danger d-flex">{{$message}}</span> @enderror

                        <div class="d-flex flex-column" id="previewHolderBox">
                            <div class="font-weight-bold mt-1">Thumbnail :</div>

                            <div class="row">
                                <div class="col-12 col-md-5">
                                    <img class=""
                                        src="https://img.youtube.com/vi/{{ youtubeID($model->youtube) }}/mqdefault.jpg"
                                        id="previewHolder">
                                </div>
                            </div>
                        </div>

                    </div>

                    <div class="form-group">
                        <label for="InputContent">Content</label>
                        <textarea class="summernote" id="InputContent"
                            name="content">{{$model->content ?? old('content')}}</textarea>
                    </div>

                    <div class="form-group">
                        <label for="InputDate">Date</label>
                        <input type="date" class="form-control @error('published_at') is-invalid @enderror"
                            id="InputDate" placeholder="Date" name="published_at"
                            value="{{ changeDateFormate($model->published_at ?? old('published_at') ,'Y-m-d') }}">
                        @error('published_at') <span class="text-danger">{{$message}}</span> @enderror
                    </div>

                    <div class="form-group">
                        <label for="InputStatus">Status</label>
                        <select name="status" id="InputStatus" class="form-control">
                            @foreach(statusNews() as $key => $value)
                            <option value="{{ $key }}" {{ $model->status == $key? "selected" : "" }}>{{ $value }}
                            </option>
                            @endforeach
                        </select>
                    </div>

                </div>

                <div class="card-footer">
                    <button type="submit" class="btn btn-primary">Update</button>
                    <a href="{{route('news.index')}}" class="btn btn-default">
                        Cancel
                    </a>
                </div>
            </div>
        </div>
    </div>
    @stop

    @push('js')
    <script type="text/javascript">
    function YouTubeGetID(url) {
        var ID = '';
        url = url.replace(/(>|<)/gi, '').split(/(vi\/|v=|\/v\/|youtu\.be\/|\/embed\/)/);
        if (url[2] !== undefined) {
            ID = url[2].split(/[^0-9a-z_\-]/i);
            ID = ID[0];
        } else {
            ID = url;
        }
        return ID;
    }

    $(document).ready(function() {

        $("#InputYoutube").keyup(function() {

            var input = $(this);

            if (input.val() == "") {
                $("#checkYoutube").attr('disabled', true);
                $('#previewHolderBox').removeClass("d-flex")
                $('#previewHolderBox').addClass("d-none")
            } else {
                $("#checkYoutube").attr('disabled', false);
            }

        })

        $("#checkYoutube").click(function() {
            const input = $("#InputYoutube").val()

            const id = YouTubeGetID(input)

            const url = 'https://img.youtube.com/vi/' + id + '/mqdefault.jpg';

            $('#previewHolder').attr('src', url);
            $('#previewHolderBox').removeClass("d-none")
            $('#previewHolderBox').addClass("d-flex")

        })

        $('.summernote').summernote();
    });
    </script>

    @endpush