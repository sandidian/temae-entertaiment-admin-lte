@extends('adminlte::page')

@section('title', 'List News')

@section('content_header')
<h1 class="m-0 text-dark">List News</h1>
@stop

@section('content')

<div class="row">
    <div class="col-12">
        <div class="card">
            <div class="card-body">

                <a href="{{route('news.create')}}" class="btn btn-primary mb-2">
                    Add
                </a>

                <table class="table table-hover table-bordered table-stripped" id="example2">
                    <thead>
                        <tr>
                            <th>No.</th>
                            <th>Title</th>
                            <th>Thumbnail</th>
                            <th>Date</th>
                            <th>Status</th>
                            <th>Option</th>
                        </tr>
                    </thead>
                    <tbody>
                        @foreach($models as $key => $model)
                        <tr>
                            <td>{{$key+1}}</td>
                            <td>{{$model->title}}</td>
                            <td>
                                <img src="https://img.youtube.com/vi/{{ youtubeID($model->youtube) }}/mqdefault.jpg"
                                    alt="" width="150">
                            </td>
                            <td>{{ changeDateFormate($model->published_at,'l, d F  Y') }}</td>
                            <td>{{ statusNews($model->status) }}</td>
                            <td>
                                <a href="{{route('news.edit', $model)}}" class="btn btn-primary btn-xs">
                                    Edit
                                </a>
                                <a href="{{route('news.destroy', $model)}}"
                                    onclick="notificationBeforeDelete(event, this)" class="btn btn-danger btn-xs">
                                    Delete
                                </a>
                            </td>
                        </tr>
                        @endforeach
                    </tbody>
                </table>

            </div>
        </div>
    </div>
</div>
@stop

@push('js')
<form action="" id="delete-form" method="post">
    @method('delete')
    @csrf
</form>
<script>
$('#example2').DataTable({
    "responsive": true,
});

function notificationBeforeDelete(event, el) {
    event.preventDefault();
    if (confirm('Are you sure you will delete the data?')) {
        $("#delete-form").attr('action', $(el).attr('href'));
        $("#delete-form").submit();
    }
}
</script>
@endpush