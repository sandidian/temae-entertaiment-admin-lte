@extends('adminlte::page')

@section('title', 'Edit Slider Home')

@section('content_header')
<h1 class="m-0 text-dark">Edit Slider Home</h1>
@stop

@section('content')
<form action="{{route('slider-home.update', $model)}}" method="post" enctype="multipart/form-data">
    @method('PUT')
    @csrf
    <div class="row">
        <div class="col-12">
            <div class="card">
                <div class="card-body">


                    <div class="form-group">
                        <label for="exampleInputName">Name</label>
                        <input type="text" class="form-control @error('name') is-invalid @enderror"
                            id="exampleInputName" placeholder="Name" name="name" value="{{$model->name ?? old('name')}}">
                        @error('name') <span class="text-danger">{{$message}}</span> @enderror
                    </div>

                    <div class="form-group">
                        <label for="exampleInputImage">Image</label>

                        <div class="custom-file">
                            <input type="file" class="custom-file-input @error('image') is-invalid @enderror"
                                id="customFile" name="image" placeholder="Image" accept="image/*">
                            <label class="custom-file-label" for="customFile">Choose file</label>
                        </div>

                        @error('image') <span class="text-danger d-flex">{{$message}}</span> @enderror

                        <img src="{{ url($model->image) }}" alt="" width="150" class="mt-4" id="previewHolder">
                    </div>


                    <div class="form-group">
                        <label for="exampleInputOrder">Order</label>
                        <select class="form-control @error('order') is-invalid @enderror" id="exampleInputOrder"
                            name="order">
                            @for ($i = 1; $i <= 5; $i++) <option value="{{ $i }}"
                                {{ $model->order == $i ? "selected" : "" }}>{{ $i }}</option>
                                @endfor
                        </select>
                        @error('order') <span class="text-danger">{{$message}}</span> @enderror
                    </div>


                </div>

                <div class="card-footer">
                    <button type="submit" class="btn btn-primary">Update</button>
                    <a href="{{route('slider-home.index')}}" class="btn btn-default">
                        Cancel
                    </a>
                </div>
            </div>
        </div>
    </div>
    @stop

    @push('js')
    <script>
    function readURL(input) {
        if (input.files && input.files[0]) {
            var reader = new FileReader();
            reader.onload = function(e) {
                $('#previewHolder').attr('src', e.target.result);
            }
            reader.readAsDataURL(input.files[0]);
        } else {
            alert('select a file to see preview');
            $('#previewHolder').attr('src', '');
        }
    }

    $(".custom-file-input").change(function() {
        readURL(this);
    });


    $(".custom-file-input").on("change", function() {
        var fileName = $(this).val().split("\\").pop();
        $(this).siblings(".custom-file-label").addClass("selected").html(fileName);
    });
    </script>

    @endpush