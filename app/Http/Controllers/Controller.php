<?php

namespace App\Http\Controllers;

use Illuminate\Foundation\Auth\Access\AuthorizesRequests;
use Illuminate\Foundation\Bus\DispatchesJobs;
use Illuminate\Foundation\Validation\ValidatesRequests;
use Illuminate\Routing\Controller as BaseController;
use File;

class Controller extends BaseController
{
    use AuthorizesRequests, DispatchesJobs, ValidatesRequests;

    public function deleteFile($file)
    {
        $array = [
            'assets/about/core-business.png',
            'assets/about/who-we-are.png',
            'assets/about/our-achievements.jpg',
            'assets/affiliates/1.svg',
            'assets/affiliates/2.svg',
            'assets/affiliates/3.svg',
            '
            assets/slider-home/wallpaperflare.com_wallpaper-2.jpg',
            'assets/slider-home/wallpaperflare.com_wallpaper-1.jpg',
            'assets/services/services-image.jpg',
            'assets/services/services-logo.png',
            'assets/services/2.jpg',
            'assets/services/1.jpg',
        ];

        if (!in_array($file, $array)) {
            if (\File::exists(public_path($file))) {
                \File::delete(public_path($file));
            }
        }

        // else {
        //     dd('File does not exists.');
        // }
    }

    function upload($file, $dir, $main_dir = 'uploads')
    {
        $fileNameImage = time() . rand() . '.' . $file->extension();

        $path = $main_dir . '/' . $dir;

        $file->move(public_path($path), $fileNameImage);

        $imageFull = $path . '/' . $fileNameImage;

        return $imageFull;
    }
}
