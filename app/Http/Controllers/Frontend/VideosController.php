<?php

namespace App\Http\Controllers\Frontend;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\News;

class VideosController extends Controller
{
    public function index()
    {
        $models = News::where(['status' => 1])
            ->take(6)
            ->orderBy('published_at', 'DESC')
            ->get();

        return view('pages.frontend.videos.index', [
            'models' => $models,
        ]);
    }

    public function more_data(Request $request)
    {
        if ($request->ajax()) {
            $skip = $request->skip;
            $take = 6;
            $models = News::where(['status' => 1])
                ->orderBy('published_at', 'DESC')
                ->skip($skip)
                ->take($take)
                ->get();

            $view = view('pages.frontend.videos.ajax-load-more', [
                'models' => $models,
            ])->render();

            return response()->json(['view' => $view]);
        } else {
            return response()->json('Direct Access Not Allowed!!');
        }
    }

    public function detail(Request $request, $slug)
    {
        $model = News::where(['status' => 1, 'slug' => $slug])->first();

        if (!$model) {
            return redirect()
                ->route('frontend.videos')
                ->with(
                    'error_message',
                    'Creator width slug ' . $slug . ' not found'
                );
        }

        $model->sosmed = json_decode($model->sosmed, true);

        $models = News::where(['status' => 1])
            ->where('slug', '!=', $slug)
            ->orderBy('published_at', 'DESC')
            ->limit(6)
            ->get();

        return view('pages.frontend.videos.detail', [
            'model' => $model,
            'models' => $models,
        ]);
    }
}
