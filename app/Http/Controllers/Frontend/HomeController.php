<?php

namespace App\Http\Controllers\Frontend;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\SliderHome;
use App\News;
use App\Creators;

class HomeController extends Controller
{
    public function index()
    {

        $slider_home = SliderHome::orderBy('order','ASC')->limit(6)->get();

        $news = News::where(['status'=>1])->orderBy('published_at','DESC')->limit(6)->get();

        $creators = Creators::where(['status'=>1])->orderBy('id','DESC')->limit(3)->get();

        return view('pages.frontend.home.index',[
            'slider_home' => $slider_home,
            'news' => $news,
            'creators' => $creators
        ]);
        
    }
}