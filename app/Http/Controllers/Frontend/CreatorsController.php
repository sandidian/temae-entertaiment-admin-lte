<?php

namespace App\Http\Controllers\Frontend;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Creators;

class CreatorsController extends Controller
{
    public function index()
    {
        $creators = Creators::where(['status' => 1])
            ->take(6)
            ->orderBy('id', 'DESC')
            ->get();

        return view('pages.frontend.creators.index', [
            'creators' => $creators,
        ]);
    }

    public function more_data(Request $request)
    {
        if ($request->ajax()) {
            $skip = $request->skip;
            $take = 6;
            $creators = Creators::where(['status' => 1])
                ->orderBy('id', 'DESC')
                ->skip($skip)
                ->take($take)
                ->get();

            $view = view('pages.frontend.creators.ajax-load-more', [
                'creators' => $creators,
            ])->render();

            return response()->json(['view' => $view]);
        } else {
            return response()->json('Direct Access Not Allowed!!');
        }
    }

    public function detail(Request $request, $slug)
    {
        $model = Creators::where(['status' => 1, 'slug' => $slug])->first();

        if (!$model) {
            return redirect()
                ->route('frontend.creators')
                ->with(
                    'error_message',
                    'Creator width slug ' . $slug . ' not found'
                );
        }

        $model->sosmed = json_decode($model->sosmed, true);

        return view('pages.frontend.creators.detail', [
            'model' => $model,
        ]);
    }
}
