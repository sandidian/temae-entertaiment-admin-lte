<?php

namespace App\Http\Controllers\Frontend;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Services;

class ServicesController extends Controller
{
    public function index()
    {
        $services = Services::where([
            'type' => 'services',
            'status' => 1,
        ])->get();

        // dd($services);

        $portofolio = Services::where([
            'type' => 'portofolio',
            'status' => 1,
        ])
        ->limit(10)
        ->get();

        $our_client = Services::where([
            'type' => 'our-client',
            'status' => 1,
        ])->get();

        return view('pages.frontend.services.index', [
            'services' => $services,
            'portofolio' => $portofolio,
            'our_client' => $our_client,
        ]);
    }

    public function detail(Request $request, $slug)
    {
        $model = Services::where([
            'type' => 'services',
            'slug' => $slug,
        ])->first();

        if (!$model) {
            return redirect()
                ->route('frontend.services')
                ->with(
                    'error_message',
                    'Creator width slug ' . $slug . ' not found'
                );
        }

        return view('pages.frontend.services.detail', [
            'model' => $model,
        ]);
    }

    public function portofolio(Request $request)
    {
        $model = Services::where([
            'type' => 'portofolio',
            'status' => 1,
        ])->get();

        if (!$model) {
            return redirect()
                ->route('frontend.services')
                ->with(
                    'error_message',
                    'Creator width slug ' . $slug . ' not found'
                );
        }

        return view('pages.frontend.services.portofolio', [
            'model' => $model,
        ]);
    }
}
