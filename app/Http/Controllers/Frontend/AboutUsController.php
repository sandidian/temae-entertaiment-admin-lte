<?php

namespace App\Http\Controllers\Frontend;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\News;
use App\About;

class AboutUsController extends Controller
{
    public function index()
    {
        $news = News::where(['status' => 1])
            ->orderBy('published_at', 'DESC')
            ->limit(2)
            ->get();

        $model = About::where(['type' => 'about'])->first();

        $ourAffiliates = About::where([
            'type' => 'about-our-affiliates',
        ])->get();

        $content = json_decode($model->content, true);

        return view('pages.frontend.about-us.index', [
            'news' => $news,
            'content' => $content,
            'ourAffiliates' => $ourAffiliates,
        ]);
    }
}
