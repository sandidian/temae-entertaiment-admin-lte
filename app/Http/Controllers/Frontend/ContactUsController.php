<?php

namespace App\Http\Controllers\Frontend;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Contact;

class ContactUsController extends Controller
{
    public function index()
    {
        $model = Contact::where(['type' => 'contact'])->first();

        $contact = json_decode($model->content, true);

        return view('pages.frontend.contact.index', [
            'model' => $model,
            'contact' => $contact,
        ]);
    }
}
