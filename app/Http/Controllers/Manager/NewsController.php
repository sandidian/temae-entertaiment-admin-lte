<?php

namespace App\Http\Controllers\Manager;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Support\Str;
use App\News;

class NewsController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $models = News::orderBy('id', 'DESC')->get();

        return view('pages.manager.news.index', [
            'models' => $models,
        ]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('pages.manager.news.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $request->validate([
            'title' => 'required',
            'content' => 'required',
            'youtube' => 'required',
            'published_at' => 'required',
        ]);

        $array = $request->only([
            'title',
            'content',
            'youtube',
            'published_at',
            'status',
        ]);

        $slug = Str::slug($request->title, '-');

        $array['slug'] = $this->incrementSlug($slug);

        $create = News::create($array);

        return redirect()
            ->route('news.index')
            ->with('success_message', 'Successfully save data');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $model = News::find($id);
        if (!$model) {
            return redirect()
                ->route('news.index')
                ->with('error_message', 'News with id ' . $id . ' not found');
        }
        return view('pages.manager.news.edit', [
            'model' => $model,
        ]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $request->validate([
            'title' => 'required',
            'content' => 'required',
            'youtube' => 'required',
            'published_at' => 'required',
        ]);

        $model = News::find($id);

        $slug = Str::slug($request->title, '-');

        $model->slug = $this->incrementSlug($slug);

        $model->title = $request->title;
        $model->content = $request->content;
        $model->youtube = $request->youtube;
        $model->published_at = $request->published_at;
        $model->status = $request->status;

        $model->save();

        return redirect()
            ->route('news.index')
            ->with('success_message', 'Successfully update data');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $model = News::find($id);

        if ($model) {
            $model->delete();
        }
        return redirect()
            ->route('news.index')
            ->with('success_message', 'Successfully delete data');
    }

    public function incrementSlug($slug)
    {
        $original = $slug;

        $count = 2;

        while (News::where(['slug' => $slug])->exists()) {
            $slug = "{$original}-" . $count++;
        }

        return $slug;
    }
}
