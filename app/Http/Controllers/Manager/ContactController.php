<?php

namespace App\Http\Controllers\Manager;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Contact;

class ContactController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $model = Contact::where(['type' => 'contact'])->first();

        $contact = json_decode($model->content, true);

        return view('pages.manager.contact.index', [
            'model' => $model,
            'contact' => $contact,
        ]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request)
    {
        $request->validate(
            [
                'name' => 'required',
                'input.location' => 'required',
                'input.phone' => 'required',
                'input.email' => 'required',
            ],
            [
                'name.required' => 'Name is required',
                'input.phone.required' => 'Phone is required',
                'input.location.required' => 'Location is required',
                'input.email.required' => 'Email is required',
            ]
        );

        $input = $request->input;

        $model = Contact::where(['type' => 'contact'])->first();

        $model->name = $request->name;

        $model->content = json_encode($request->input);

        $model->save();

        return redirect()
            ->route('contact.index')
            ->with('success_message', 'Successfully update data');
    }
}
