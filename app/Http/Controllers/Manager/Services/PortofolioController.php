<?php

namespace App\Http\Controllers\Manager\Services;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Services;

class PortofolioController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $models = Services::where([
            'type' => 'portofolio',
        ])->get();

        return view('pages.manager.services.portofolio.index', [
            'models' => $models,
        ]);
    }

    public function create()
    {
        return view('pages.manager.services.portofolio.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $request->validate([
            'name' => 'required',
            'image' => 'required',
            'link' => 'required',
        ]);

        $array = $request->only(['name', 'link', 'status']);

        $array['type'] = 'portofolio';

        $array['image'] = $this->upload($request->image, 'services');

        $create = Services::create($array);

        return redirect()
            ->route('portofolio.index')
            ->with('success_message', 'Successfully save data');
    }

    public function edit($id)
    {
        $model = Services::where([
            'type' => 'portofolio',
            'id' => $id,
        ])->first();

        if (!$model) {
            return redirect()
                ->route('portofolio.index')
                ->with(
                    'error_message',
                    'seravice width id ' . $id . ' not found'
                );
        }

        return view('pages.manager.services.portofolio.edit', [
            'model' => $model,
        ]);
    }

    public function update(Request $request, $id)
    {
        $request->validate([
            'name' => 'required',
            'description' => 'required',
        ]);

        $model = Services::find($id);

        $model->name = $request->name;
        $model->link = $request->link;
      
        $model->status = $request->status;

        dd($request->all());

        $oldImage = $model->image;

        if ($request->hasFile('image')) {
            $model->image = $this->upload($request->image, 'services');

            $this->deleteFile($oldImage);
        } else {
            $model->image = $oldImage;
        }


        $model->save();

        return redirect()
            ->route('portofolio.index')
            ->with('success_message', 'Successfully update data');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $model = Services::find($id);

        if ($model) {
            $this->deleteFile($model->image);
            $model->delete();
        }
        return redirect()
            ->route('portofolio.index')
            ->with('success_message', 'Successfully delete data');
    }
}
