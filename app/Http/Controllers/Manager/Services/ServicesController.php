<?php

namespace App\Http\Controllers\Manager\Services;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Support\Str;
use App\Services;

class ServicesController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $models = Services::where([
            'type' => 'services',
        ])->get();

        // dd($models);

        return view('pages.manager.services.services.index', [
            'models' => $models,
        ]);
    }

    public function create()
    {
        return view('pages.manager.services.services.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $request->validate([
            'name' => 'required',
            'image' => 'required',
            'description' => 'required',
        ]);

        $array = $request->only(['nane', 'description', 'status']);

        $slug = Str::slug($request->name, '-');

        $array['slug'] = $this->incrementSlug($slug);
        $array['type'] = 'services';

        $array['description'] = $request->description;

        $array['image'] = $this->upload($request->image, 'services');

        // dd($array);

        $create = Services::create($array);

        return redirect()
            ->route('services.index')
            ->with('success_message', 'Successfully save data');
    }

    public function edit($id)
    {
        $model = Services::where([
            'type' => 'services',
            'id' => $id,
        ])->first();

        if (!$model) {
            return redirect()
                ->route('services.index')
                ->with(
                    'error_message',
                    'seravice width id ' . $id . ' not found'
                );
        }

        return view('pages.manager.services.services.edit', [
            'model' => $model,
        ]);
    }

    public function update(Request $request, $id)
    {
        $request->validate([
            'name' => 'required',
            'description' => 'required',
        ]);

        $model = Services::find($id);

        $model->name = $request->name;
        $model->description = $request->description;
        $model->status = $request->status;

        $slug = Str::slug($request->name, '-');

        $model->slug = $this->incrementSlug($slug);

        $oldImage = $model->image;

        if ($request->hasFile('image')) {
            $model->image = $this->upload(
                $request->image,
                'services',
            );

            $this->deleteFile($oldImage);
        } else {
            $model->image = $oldImage;
        }

        $model->save();

        return redirect()
            ->route('services.index')
            ->with('success_message', 'Successfully update data');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $model = Services::find($id);

        if ($model) {
            $this->deleteFile($model->image);
            $model->delete();
        }
        return redirect()
            ->route('services.index')
            ->with('success_message', 'Successfully delete data');
    }

    public function incrementSlug($slug)
    {
        $original = $slug;

        $count = 2;

        while (Services::where(['slug' => $slug])->exists()) {
            $slug = "{$original}-" . $count++;
        }

        return $slug;
    }

}
