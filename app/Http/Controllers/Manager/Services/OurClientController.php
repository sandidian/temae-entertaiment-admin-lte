<?php

namespace App\Http\Controllers\Manager\Services;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Services;

class OurClientController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $models = Services::where([
            'type' => 'our-client',
        ])->get();

        // dd($models);

        return view('pages.manager.services.our-client.index', [
            'models' => $models,
        ]);
    }

    public function create()
    {
        return view('pages.manager.services.our-client.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $request->validate([
            'name' => 'required',
            'link' => 'required',
        ]);

        $array = $request->only(['name', 'link', 'status']);

        $array['type'] = 'our-client';

        $create = Services::create($array);

        return redirect()
            ->route('our-client.index')
            ->with('success_message', 'Successfully save data');
    }

    public function edit($id)
    {
        $model = Services::where([
            'type' => 'our-client',
            'id' => $id,
        ])->first();
        

        if (!$model) {
            return redirect()
                ->route('our-client.index')
                ->with(
                    'error_message',
                    'seravice width id ' . $id . ' not found'
                );
        }

        return view('pages.manager.services.our-client.edit', [
            'model' => $model,
        ]);
    }

    public function update(Request $request, $id)
    {
        $request->validate([
            'name' => 'required',
            'link' => 'required',
        ]);

        $model = Services::find($id);

        $model->name = $request->name;
        $model->link = $request->link;
        $model->status = $request->status;

        $model->save();

        return redirect()
            ->route('our-client.index')
            ->with('success_message', 'Successfully update data');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $model = Services::find($id);

        if ($model) {
            $model->delete();
        }
        return redirect()
            ->route('our-client.index')
            ->with('success_message', 'Successfully delete data');
    }

}
