<?php

namespace App\Http\Controllers\Manager\About;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\About;

class CoreBusinessController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $model = About::where(['type' => 'about'])->first();

        $about = json_decode($model->content, true);

        return view('pages.manager.about.core-business.index', [
            'about' => $about,
        ]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request)
    {
        $request->validate(
            [
                'input.description' => 'required',
            ],
            [
                'input.description.required' => 'Description is required',
            ]
        );

        $input = $request->input;

        $model = About::where(['type' => 'about'])->first();

        $about = json_decode($model->content, true);

        $oldImage = $input['image'];

        if ($request->hasFile('image')) {
            $input['image'] = $this->upload($request->image, 'about');

            $this->deleteFile($oldImage);
        } else {
            $input['image'] = $oldImage;
        }

        $about['core_business'] = $input;

        $model->content = json_encode($about);

        $model->save();

        return redirect()
            ->route('core-business.index')
            ->with('success_message', 'Successfully update data');
    }
}
