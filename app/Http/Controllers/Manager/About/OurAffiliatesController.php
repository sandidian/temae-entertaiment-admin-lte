<?php

namespace App\Http\Controllers\Manager\About;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\About;

class OurAffiliatesController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $models = About::where([
            'type' => 'about-our-affiliates',
        ])->get();

        return view('pages.manager.about.our-affiliates.index', [
            'models' => $models,
        ]);
    }

    public function create()
    {
        return view('pages.manager.about.our-affiliates.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $request->validate([
            'name' => 'required',
            'image' => 'required',
        ]);

        $array = $request->only(['name', 'status']);

        $array['image'] = $this->upload($request->image, 'about');

        $array['type'] = 'about-our-affiliates';

        $create = About::create($array);

        return redirect()
            ->route('our-affiliates.index')
            ->with('success_message', 'Successfully save data');
    }

    public function edit($id)
    {
        $model = About::where([
            'type' => 'about-our-affiliates',
            'id' => $id,
        ])->first();

        if (!$model) {
            return redirect()
                ->route('our-affiliates.index')
                ->with(
                    'error_message',
                    'seravice width id ' . $id . ' not found'
                );
        }

        return view('pages.manager.about.our-affiliates.edit', [
            'model' => $model,
        ]);
    }

    public function update(Request $request, $id)
    {
        $request->validate([
            'name' => 'required',
        ]);

        $model = About::find($id);

        $model->name = $request->name;

        $oldImage = $model->image;

        if ($request->hasFile('image')) {
            $model->image = $this->upload($request->image, 'about');

            $this->deleteFile($oldImage);
        } else {
            $model->image = $oldImage;
        }

        $model->status = $request->status;

        $model->save();

        return redirect()
            ->route('our-affiliates.index')
            ->with('success_message', 'Successfully update data');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $model = About::find($id);

        if ($model) {
            $this->deleteFile($model->image);
            $model->delete();
        }
        return redirect()
            ->route('our-affiliates.index')
            ->with('success_message', 'Successfully delete data');
    }
}
