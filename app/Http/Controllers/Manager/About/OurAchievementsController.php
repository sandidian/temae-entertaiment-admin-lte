<?php

namespace App\Http\Controllers\Manager\About;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\About;

class OurAchievementsController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $model = About::where(['type' => 'about'])->first();

        $about = json_decode($model->content, true);

        return view('pages.manager.about.our-achievements.index', [
            'about' => $about,
        ]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request)
    {
        $input = $request->input;

        $model = About::where(['type' => 'about'])->first();

        $about = json_decode($model->content, true);

        $oldImage = $input['image'];

        if ($request->hasFile('image')) {
            $input['image'] = $this->upload($request->image, 'about');

            $this->deleteFile($oldImage);
        } else {
            $input['image'] = $oldImage;
        }

        $about['our_achievements'] = $input;

        $model->content = json_encode($about);

        $model->save();

        return redirect()
            ->route('our-achievements.index')
            ->with('success_message', 'Successfully update data');
    }
}
