<?php

namespace App\Http\Controllers\Manager;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\SliderHome;

class SliderHomeController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $models = SliderHome::all();
        return view('pages.manager.slider-home.index', [
            'models' => $models,
        ]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('pages.manager.slider-home.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $request->validate([
            'name' => 'required',
            'image' => 'required|mimes:png,jpg,jpeg',
            'order' => 'required',
        ]);

        $fileName = time() . '.' . $request->image->extension();

        $path = 'uploads/slider-home';

        $request->image->move(public_path($path), $fileName);

        $imageFull = $path . '/' . $fileName;

        $array = $request->only(['name', 'order']);

        $array['image'] = $imageFull;

        $create = SliderHome::create($array);

        return redirect()
            ->route('slider-home.index')
            ->with('success_message', 'Successfully save data');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $model = SliderHome::find($id);
        if (!$model) {
            return redirect()
                ->route('slider-home.index')
                ->with(
                    'error_message',
                    'Slider home with id ' . $id . ' not found'
                );
        }
        return view('pages.manager.slider-home.edit', [
            'model' => $model,
        ]);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $model = SliderHome::find($id);
        if (!$model) {
            return redirect()
                ->route('slider-home.index')
                ->with(
                    'error_message',
                    'Slider home with id ' . $id . ' not found'
                );
        }
        return view('pages.manager.slider-home.edit', [
            'model' => $model,
        ]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $request->validate([
            'name' => 'required',
            'order' => 'required',
        ]);

        $model = SliderHome::find($id);

        $oldImage = $model->image;

        if ($request->hasFile('image')) {
            $fileName = time() . '.' . $request->image->extension();

            $path = 'uploads/slider-home';

            $request->image->move(public_path($path), $fileName);

            $imageFull = $path . '/' . $fileName;

            $model->image = $imageFull;

            $this->deleteFile($oldImage);
        } else {
            $model->image = $oldImage;
        }

        $model->name = $request->name;
        $model->order = $request->order;

        $model->save();

        return redirect()
            ->route('slider-home.index')
            ->with('success_message', 'Successfully update data');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $model = SliderHome::find($id);

        if ($model) {
            $this->deleteFile($model->image);
            $model->delete();
        }
        return redirect()
            ->route('slider-home.index')
            ->with('success_message', 'Successfully delete data');
    }
}