<?php

namespace App\Http\Controllers\Manager;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Support\Str;
use App\Creators;

class CreatorsController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $models = Creators::orderBy('id', 'DESC')->get();

        return view('pages.manager.creators.index', [
            'models' => $models,
        ]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('pages.manager.creators.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $request->validate([
            'name' => 'required',
            'description' => 'required',
            'image' => 'required',
            'image_cover' => 'required',
        ]);

        $array = $request->only(['name', 'description', 'status']);

        $slug = Str::slug($request->name, '-');

        $array['slug'] = $this->incrementSlug($slug);

        $array['image'] = $this->upload($request->image, 'creator-image');
        $array['image_cover'] = $this->upload(
            $request->image_cover,
            'creator-image-cover'
        );

        $array['sosmed'] = json_encode($request->sosmed);

        // dd($array);

        $create = Creators::create($array);

        return redirect()
            ->route('creators.index')
            ->with('success_message', 'Successfully save data');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $model = Creators::find($id);

        if (!$model) {
            return redirect()
                ->route('creators.index')
                ->with(
                    'error_message',
                    'Creator width id ' . $id . ' not found'
                );
        }

        $model->sosmed = json_decode($model->sosmed, true);

        return view('pages.manager.creators.edit', [
            'model' => $model,
        ]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $request->validate([
            'name' => 'required',
            'description' => 'required',
        ]);

        $model = Creators::find($id);

        $model->name = $request->name;
        $model->description = $request->description;
        $model->status = $request->status;

        $slug = Str::slug($request->name, '-');

        $model->slug = $this->incrementSlug($slug);

        $oldImage = $model->image;

        if ($request->hasFile('image')) {
            $model->image = $this->upload($request->image, 'creator-image');

            $this->deleteFile($oldImage);
        } else {
            $model->image = $oldImage;
        }

        $oldImageCover = $model->image_cover;

        if ($request->hasFile('image_cover')) {
            $model->image_cover = $this->upload(
                $request->image,
                'creator-image'
            );

            $this->deleteFile($oldImageCover);
        } else {
            $model->image_cover = $oldImageCover;
        }

        $model->sosmed = json_encode($request->sosmed);

        $model->save();

        return redirect()
            ->route('creators.index')
            ->with('success_message', 'Successfully update data');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $model = Creators::find($id);

        if ($model) {
            $this->deleteFile($model->image);
            $this->deleteFile($model->image_cover);
            $model->delete();
        }
        return redirect()
            ->route('creators.index')
            ->with('success_message', 'Successfully delete data');
    }

    public function incrementSlug($slug)
    {
        $original = $slug;

        $count = 2;

        while (Creators::where(['slug' => $slug])->exists()) {
            $slug = "{$original}-" . $count++;
        }

        return $slug;
    }
}
