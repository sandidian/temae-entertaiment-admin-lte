<?php

namespace App\Http\Controllers\Manager;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\About;

class AboutController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $model = About::where(['id' => 1])->first();

        $about = json_decode($model->content, true);

        // dd($about);

        return view('pages.manager.about.index', [
            'about' => $about,
        ]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request)
    {
        $request->validate(
            [
                'about.who_we_are.description' => 'required',
                'about.core_business.description' => 'required',
            ],
            [
                'about.who_we_are.description.required' =>
                    'Description is required',
                'about.core_business.description.required' =>
                    'Description is required',
            ]
        );

        $model = About::where(['id' => 1])->first();

        $content = $request->about;

        $oldImageWhoWeAre = $content['who_we_are']['image'];

        if ($request->hasFile('whoWeAreImage')) {
            $content['who_we_are']['image'] = $this->upload(
                $request->whoWeAreImage,
                'about'
            );

            $this->deleteFile($oldImageWhoWeAre);
        } else {
            $content['who_we_are']['image'] = $oldImageWhoWeAre;
        }

        $oldImageOurAchievements = $content['our_achievements']['image'];

        if ($request->hasFile('ourAchievementsImage')) {
            $content['our_achievements']['image'] = $this->upload(
                $request->ourAchievementsImage,
                'about'
            );

            $this->deleteFile($oldImageOurAchievements);
        } else {
            $content['our_achievements']['image'] = $oldImageOurAchievements;
        }

        $oldImageCoreBusinessImage = $content['core_business']['image'];

        if ($request->hasFile('coreBusinessImage')) {
            $content['core_business']['image'] = $this->upload(
                $request->coreBusinessImage,
                'about'
            );

            $this->deleteFile($oldImageCoreBusinessImage);
        } else {
            $content['core_business']['image'] = $oldImageCoreBusinessImage;
        }

        $model->content = json_encode($content);

        $model->save();

        return redirect()
            ->route('about.index')
            ->with('success_message', 'Successfully update data');
    }

    function upload($file, $dir)
    {
        $fileNameImage = time() . rand() . '.' . $file->extension();

        $path = 'assets/' . $dir;

        $file->move(public_path($path), $fileNameImage);

        $imageFull = $path . '/' . $fileNameImage;

        return $imageFull;
    }

    public function addField(Request $request)
    {
        if ($request->ajax()) {
            $no = $request->skip;
            $take = 3;
            $creators = Creators::where(['status' => 1])
                ->orderBy('id', 'DESC')
                ->skip($skip)
                ->take($take)
                ->get();

            $view = view('pages.frontend.creators.ajax-load-more', [
                'creators' => $creators,
            ])->render();

            return response()->json(['view' => $view]);
        } else {
            return response()->json('Direct Access Not Allowed!!');
        }
    }
}
