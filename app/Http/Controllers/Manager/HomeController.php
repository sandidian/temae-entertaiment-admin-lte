<?php

namespace App\Http\Controllers\Manager;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Hash;
use App\User;
use Auth;

class HomeController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Contracts\Support\Renderable
     */
    public function index()
    {
        return view('pages.manager.home.index');
    }

    public function profile()
    {
        $model = Auth::user();

        return view('pages.manager.home.profile', ['model' => $model]);
    }

    public function profileUpdate(Request $request)
    {
        $model = Auth::user();

        $rules = [];

        $rules['name'] = 'required';

        if ($request->email != $model->email) {
            $rules['email'] = 'unique:users';
        }

        $request->validate($rules);

        $model->name = $request->name;
        $model->email = $request->email;

        return redirect()
            ->route('home.profile')
            ->with('success_message', 'Successfully update data');
    }

    public function changePassword()
    {
        $model = Auth::user();

        return view('pages.manager.home.change-password', ['model' => $model]);
    }

    public function changePasswordUpdate(Request $request)
    {
        $user = Auth::user();

        $model = User::findOrFail($user->id);

        $rules = [
            'password' => 'required',
            'new_password' => 'min:8|different:password',
        ];

        $request->validate($rules);

        if (Hash::check($request->password, $model->password)) {
            $model
                ->fill([
                    'password' => Hash::make($request->new_password),
                ])
                ->save();

            return redirect()
                ->route('home.change-password')
                ->with('success_message', 'Successfully update data');
        } else {
            return redirect()
                ->route('home.change-password')
                ->with('error_message', 'Old Password does not match');
        }
    }
}
