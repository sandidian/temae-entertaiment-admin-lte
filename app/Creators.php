<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Creators extends Model
{
    protected $table = 'creators';

    protected $fillable = [
        'name',
        'description',
        'slug',
        'image',
        'image_cover',
        'sosmed',
        'status',
    ];
}