<?php

function statusNews($id = null)
{
    $data = [1 => 'Publish', 2 => 'Not Publish'];

    if (empty($id)) {
        return $data;
    }

    return $data[$id];
}

function sosmedObj($data)
{
    return json_decode($data, true);
}

function sosmed($id = null)
{
    $data = [
        'youtube' => 'Youtube',
        'facebook' => 'Facebook',
        'instagram' => 'Instagram',
        'twitter' => 'Twitter',
    ];

    if (empty($id)) {
        return $data;
    }

    return $data[$id];
}

function youtubeID($url)
{
    if (strlen($url) > 11) {
        if (
            preg_match(
                '%(?:youtube(?:-nocookie)?\.com/(?:[^/]+/.+/|(?:v|e(?:mbed)?)/|.*[?&]v=)|youtu\.be/)([^"&?/ ]{11})%i',
                $url,
                $match
            )
        ) {
            return $match[1];
        } else {
            return false;
        }
    }

    return $url;
}

function changeDateFormate($date, $format)
{
    return \Carbon\Carbon::parse($date)->format($format);
}

function arrOurClient($id = null)
{
    $data = [
        'sunrise.png',
        'tokopedia.png',
        'cimory.png',
        'disney.png',
        'prabu.png',
        'maxim.png',
    ];

    if (empty($id)) {
        return $data;
    }

    return $data[$id];
}

?>
