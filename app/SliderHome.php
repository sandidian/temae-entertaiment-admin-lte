<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class SliderHome extends Model
{
    protected $table = 'slider_home';

    protected $fillable = [
        'name', 'image', 'order',
    ];
}
