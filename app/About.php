<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class About extends Model
{
    protected $table = 'layouts';

    protected $fillable = ['type', 'name', 'content', 'image','status'];
}
